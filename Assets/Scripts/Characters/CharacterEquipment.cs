﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//скрипт снаряжения персонажа
//висит на персонаже

    //Не делать поля приватными, пока работаем с ними из инспектора!!!

public class CharacterEquipment : MonoBehaviour
{
    public Item head;
    public Item pauldron;
    public Item chest;
    public Item legs;
    public Item hands;
    public Item feet;
    public Item neck;
    public Item ring_01;
    public Item ring_02;
    public Bag bag;
    public Item belt;
    public Item accessory;
    public Weapon rightWeapon;
    public Weapon leftWeapon;

}
