﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;

//это скрипт профиля NPC
//висит на соответствующем объекте

public class CharacterProfile : MonoBehaviour
{
    [Header("Общее")]
    [SerializeField]
    string characterName = "";//имя персонажа
    [SerializeField]
    string characterLabelName = "";//имя персонажа, выводимое на лейбле
    [SerializeField]
    string characterGender = "";//пол персонажа

    [SerializeField]
    bool inFocus = false;//этот персонаж в фокусе (выбран)
    [SerializeField]
    bool isDead = false;//этот персонаж мертв

    [SerializeField]
    int characterID;//идентификатор персонажа
    [SerializeField]
    int characterLevel;//уровень персонажа
    [SerializeField]
    int maxHealh = 100;//его максимальный показатель здоровья
    [SerializeField]
    int currentHealh = 100;//и текущий процент

    [Header("Аватары")]
    [SerializeField]
    GameObject characterAvatar;//аватар 1/1
    [SerializeField]
    GameObject characterPict;//аватар 2/3

    [Header("Диалог")]
    [SerializeField]
    int startDialogHere = 0;//идентификатор реплики, с которой будет начинаться диалог с этим персонажем
    [SerializeField]
    bool dialogEnable = true;//доступность диалога в настоящий момент времени либо для конкретного персонажа

    [Header("первичные характеристики")]
    [SerializeField]
    int strBase;//сила
    [SerializeField]
    int dexBase;//ловкость
    [SerializeField]
    int constBase;//телосложение
    [SerializeField]
    int intBase;//интеллект
    [SerializeField]
    int percBase;//восприятие

    [Header("модификаторы характеристик")]
    [SerializeField]
    int strMod;//сила
    [SerializeField]
    int dexMod;//ловкость
    [SerializeField]
    int constMod;//тело
    [SerializeField]
    int intMod;//интеллект
    [SerializeField]
    int percMod;//восприятие

    [Header("вторичные характеристики")]
    [SerializeField]
    int weightLimit;//лимит переносимого веса

    //для локализации лейбла
    TextAsset asset;
    XMLSettings labelTXT;

    public string CharacterName { get => characterName; set => characterName = value; }
    public string CharacterLabelName { get => characterLabelName; set => characterLabelName = value; }
    public string CharacterGender { get => characterGender; set => characterGender = value; }
    public int CharacterID { get => characterID; set => characterID = value; }
    public int CharacterLevel { get => characterLevel; set => characterLevel = value; }
    public int StartNode { get => startDialogHere; set => startDialogHere = value; }
    public GameObject CharacterAvatar { get => characterAvatar; set => characterAvatar = value; }
    public GameObject CharacterPict { get => characterPict; set => characterPict = value; }
    public int MaxHealh { get => maxHealh; set => maxHealh = value; }
    public int CurrentHealh { get => currentHealh; set => currentHealh = value; }
    public bool DialogEnable { get => dialogEnable; set => dialogEnable = value; }
    public bool InFocus { get => inFocus; set => inFocus = value; }
    public bool IsDead { get => isDead; set => isDead = value; }
    public int StrBase { get => strBase; set => strBase = value; }
    public int DexBase { get => dexBase; set => dexBase = value; }
    public int ConstBase { get => constBase; set => constBase = value; }
    public int IntBase { get => intBase; set => intBase = value; }
    public int PercBase { get => percBase; set => percBase = value; }
    public int StrMod { get => strMod; set => strMod = value; }
    public int DexMod { get => dexMod; set => dexMod = value; }
    public int ConstMod { get => constMod; set => constMod = value; }
    public int IntMod { get => intMod; set => intMod = value; }
    public int PercMod { get => percMod; set => percMod = value; }
    public int WeightLimit { get => weightLimit; set => weightLimit = value; }

    void Awake()
    {
        CharacterAvatar = Resources.Load<GameObject>("Prefabs/UIPrefabs/Avatars/" + CharacterName);
        CharacterPict = Resources.Load<GameObject>("Prefabs/UIPrefabs/Character Pictures/" + CharacterName);
        ApplyStatistics();
    }

    public void ApplyLabelName()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Character Labels");
        labelTXT = XMLSettings.Load(asset);
        CharacterLabelName = labelTXT.CharacterLabels[CharacterID].text;
    }

    public void ApplyStatistics()
    {
        //расчет модификаторов
        strMod = strBase / 3;
        dexMod = dexBase / 3;
        intMod = intBase / 3;
        percMod = percBase / 3;

        //расчет переносимого веса для Кореда
        if (CharacterName == "Kored")
        {
            weightLimit = StrBase * 5;
        }
        //расчет переносимого веса для остальных персонажей
        else
        {
            weightLimit = StrBase * 10;
        }
    }


    //для сохранения
    public XElement GetElement()
    {
        //имя персонажа
        XAttribute charName = new XAttribute("name", CharacterName);

        //местоположение персонажа
        XAttribute x = new XAttribute("x", transform.position.x);
        XAttribute y = new XAttribute("y", transform.position.y);
        XAttribute z = new XAttribute("z", transform.position.z);

        //характеристики персонажа
        XAttribute charStr = new XAttribute("str", StrBase);
        XAttribute charDex = new XAttribute("dex", DexBase);
        XAttribute charConst = new XAttribute("const", ConstBase);
        XAttribute charInt = new XAttribute("int", IntBase);
        XAttribute charPerc = new XAttribute("perc", PercBase);

        XElement element = new XElement("member", name, x, y, z, charStr, charDex, charConst, charInt, charPerc);
        return element;
    }

}
