﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//висит на объекте, содержащем в себе главную камеру

public class CameraMovement : MonoBehaviour
{
    public int speed = 50;

    public GameObject cam;

    public float leftPos, rightPos, upPos, downPos;

    //чтобы можно было блокировать перемещение камеры
    public bool camScroll = true;
    public bool camMove = true;

    public float minCam, maxCam;

    void Update()
    {
        float z = Input.GetAxis("Mouse ScrollWheel");//считываем инфу с колеса мыши

        if (z != 0 && camScroll)//если крутим колесо мыши и приближение/отдаление камеры разрешено,
        {
            cam.transform.Translate(new Vector3(0, 0, z * speed / 2));//меняем координаты камеры
            //ограничиваем перемещение камеры осью y и максимальным и минимальным значениями
            cam.transform.localPosition = new Vector3(cam.transform.localPosition.x, Mathf.Clamp(cam.transform.localPosition.y, minCam, maxCam), cam.transform.localPosition.z);
        }

        if (camMove)//если разрешено перемещение камеры, то считываем местонахждение мыши по отношению к стороне/углу экрана и перемещаем контейнер, содержащий камеру (не саму камеру!) туда
        {
            if ((transform.position.x >= leftPos) && ((int)Input.mousePosition.x < 2))
            {
                transform.position -= transform.right * Time.deltaTime * speed;
            }
            if ((transform.position.x <= rightPos) && ((int)Input.mousePosition.x > Screen.width - 2))
            {
                transform.position += transform.right * Time.deltaTime * speed;
            }
            if ((transform.position.z <= upPos) && (Input.mousePosition.y > Screen.height - 2))
            {
                transform.position += transform.forward * Time.deltaTime * speed;
            }
            if ((transform.position.z >= downPos) && (Input.mousePosition.y < 2))
            {
                transform.position -= transform.forward * Time.deltaTime * speed;
            }
        }
    }
}
