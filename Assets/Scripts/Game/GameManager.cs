﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;//искин для работы навмеша
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ThePlace;

//террейн или плейн должен быть на слое Ground

public class GameManager : Singleton<GameManager>
{
    //репутация у богов
    public static int EchoReputation = 0;
    public static int ShimrathReputation = 0;
    public static int CloudReputation = 0;
    public static int CalebReputation = 0;

    public GameObject dialoguePanel;
    public GameObject target;//цель

    public InventoryPanelManager inventory;

    GroupManager group;
    GameObject selectedMember;
    LayerMask Ground;//по чему мы можем ходить

    void Awake()
    {
        Ground = LayerMask.GetMask("Ground");
        group = FindObjectOfType<GroupManager>();
        inventory = FindObjectOfType<InventoryPanelManager>();
    }

    void Update()
    {
        //при клике
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())//проверка: не "висит" ли курсор над элементом UI
        {
            TaskManager.StopAllTasks(this);//останавливаем все предыдущие задачи

            Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);//пускаем луч
            RaycastHit hit;//запоминаем его координаты

            if (Physics.Raycast(myRay, out hit, 100, Ground))//если луч попал на землю
            {
                selectedMember.GetComponent<NavMeshAgent>().SetDestination(hit.point);
            }

            if (Physics.Raycast(myRay, out hit))//выясняем, на что мы кликнули
            {
                target = hit.transform.gameObject;
                if (target.GetComponent<ItemComponent>() != null)//если это предмет, который можно подобрать
                {
                    TaskManager.PlayTasks(this,
                                            MoveTo(target.transform.position),  //подойти к предмету
                                            TakeItem(target));                  //взять предмет
                }
                else if (target.GetComponent<TeleportProfile>() != null)//если это телепорт
                {
                    TaskManager.PlayTasks(this,
                                            MoveTo(target.transform.position),  //подойти к телепорту
                                            TeleportThere(target));//телепортироваться
                }
                else if (target.GetComponent<CharacterProfile>() != null)//если это npc
                {
                    TaskManager.PlayTasks(this,
                                            MoveTo(target.transform.position), //подойти к npc
                                            StartDialog(target));//начать диалог
                }
            }
        }
    }

    //задача: приблизиться к цели
    IEnumerator MoveTo(Vector3 dest)//сюда передаем позицию
    {
        float distance = Vector3.Magnitude(dest - selectedMember.transform.position);//измеряем расстояние до цели
        while (distance > 2f)//пока расстояние больше x
        {
            selectedMember.GetComponent<NavMeshAgent>().SetDestination(dest);//двигаем персонажа туда
            distance = Vector3.Magnitude(dest - selectedMember.transform.position);//и измеряем расстояние до цели
            yield return null;
        }
    }

    //задача: подобрать предмет
    IEnumerator TakeItem(GameObject item)//сюда передаем итем
    {
        Item newItem = item.GetComponentInParent<ItemComponent>().item;
        if (FindObjectOfType<InventoryPanelManager>().FindAndAddItem(newItem))//вызываем метод поиска аналогичного предмета в инвентаре, и, если таковый нашелся, то добавляем подбираемый итем в ту же ячейку
        {
            Destroy(item);//и сам итем
        }
        else//а если нет
        {
            if (FindObjectOfType<InventoryPanelManager>().AddNewItem(newItem))//тогда ищем пустую ячейку и помещаем предмет в нее
            {
                //если инвентарь был не переполнен (нашлась свободная ячейка), то метод сработал и можно...
                Destroy(item);//и сам итем
            }
        }
        inventory.UpdateGroupWeight();
        yield return null;
    }

    //задача: телепортироваться (переместиться)
    IEnumerator TeleportThere(GameObject target)
    {
        GameObject destination = target.GetComponent<TeleportProfile>().destination;//заглядываем в профиль телепорта и берем оттуда точку назначения
        selectedMember.GetComponent<NavMeshAgent>().enabled = false;//отключаем навмешагент (патамушто баги!)
        selectedMember.transform.position = destination.transform.position;//телепортируемся
        selectedMember.GetComponent<NavMeshAgent>().enabled = true;//включаем навмешагент обратно
        yield return null;
    }

    //задача: начать диалог
    IEnumerator StartDialog(GameObject target)
    {
        CharacterProfile profile = target.GetComponent<CharacterProfile>();
        if (profile.DialogEnable == true)//если диалог с этим неписем в настоящее время доступен...
        {
            dialoguePanel.SetActive(true);//включаем панель диалога
            UIManager.dialogPanelIsActive = true;
            DialogManager dialog = FindObjectOfType<DialogManager>();//находим на ней скрипт Dialog
            dialog.fileName = target.GetComponent<CharacterProfile>().CharacterName;
            dialog.id = target.GetComponent<CharacterProfile>().StartNode;//передаем ноду, с которой следует начать диалог
            dialog.UpdateDialog();
        }
        yield return null;
    }

    public void UpdateSelectedMember()
    {
        //определяем выбранного мембера
        for (int i = 0; i < group.groupMembersCount; i++)
        {
            if (group.members[i].GetComponent<CharacterProfile>().InFocus == true)
            {
                selectedMember = group.members[i];
            }
        }
    }

}


