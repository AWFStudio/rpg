﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ThePlace;
using System.IO;
using System.Xml.Linq;


public class SaveManager : Singleton<SaveManager>
{
    public static string path;

    public static string[] saves;

    private void Awake()
    {
        saves = new string[10];
    }

    public static void Save(XElement element)
    {
        for (int i = 0; i < saves.Length; i++)
        {
            if (saves[i] == null)
            {
                path = Application.persistentDataPath + "/save" + i.ToString() + ".xml";
                saves[i] = path;
                XElement root = new XElement("root");
                root.Add(element);
                XDocument saveDoc = new XDocument(root);
                File.WriteAllText(path, saveDoc.ToString());
            }
        }
    }





    //void Load()
    //{
    //    XElement root = null;//создаем элемент root
    //    if (File.Exists(path))//если мы нашли файл с сейвом
    //    {
    //        root = XDocument.Parse(File.ReadAllText(path)).Element("root");//распарсим его в элемент root
    //    }
    //    ApplyLoad(root);
    //}

    //void ApplyLoad(XElement root)
    //{
    //    playerCharacter = root.Element("members").Element("member").Attribute("name").Value;
    //}

}
