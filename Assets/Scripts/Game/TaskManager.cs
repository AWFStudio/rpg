﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Класс, который начинает проигрывать список заданий (корутин) для конкретного объекта
// висит на пустом объекте сцены

public class TaskManager : MonoBehaviour
{
    //это массив ссылок на те объекты, которым был назначен список заданий на выполнение.
    //этот массив нужен для того, чтобы потом была возможность остановить выполнение заданий в любой момент.
    static List<TaskObject> currentTargets = new List<TaskObject>();

    // PUBLIC
    //этот метод принимает объект и задания и начинает их выполнять
    public static void PlayTasks(MonoBehaviour targetObject, params IEnumerator[] tasks)
    {
        TaskObject taskObject = new TaskObject(targetObject, tasks, currentTargets.Count);
        currentTargets.Add(taskObject);
        targetObject.StartCoroutine(playTasks(taskObject));
    }
    // PUBLIC
    // этот метод останавливает выполнение всех заданий для конкретного объекта
    public static void StopAllTasks(MonoBehaviour targetObject)
    {
        for (int i = currentTargets.Count - 1; i >= 0; i--)
        {
            if (currentTargets[i].target == targetObject)
            {
                foreach (IEnumerator coroutine in currentTargets[i].tasks)
                {
                    targetObject.StopCoroutine(coroutine);
                }
                currentTargets.RemoveAt(i);
            }
        }
    }

    // приватный метод выполнения заданий. Когда задания будут выполнены, то список заданий для этого объекта удаляется
    static IEnumerator playTasks(TaskObject taskObject)
    {

        for (int i = 0; i < taskObject.tasks.Length; i++)
            yield return taskObject.target.StartCoroutine(taskObject.tasks[i]);

        currentTargets.RemoveAt(taskObject.id);
    }

    //вспомогательный класс, который содержит в себе цель (для заданий), массив заданий и индекс места в массиве currentTargets, где он будет находится
    class TaskObject
    {
        public MonoBehaviour target;
        public IEnumerator[] tasks;
        public int id;

        public TaskObject(MonoBehaviour target, IEnumerator[] tasks, int id)
        {
            this.target = target;
            this.tasks = tasks;
            this.id = id;
        }
    }

}

