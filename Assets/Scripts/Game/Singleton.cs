﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//это шаблон для создания всех синглтонов
//он проверяет, есть ли уже такой синглтон, и если есть, то не дает создать его клон

namespace ThePlace
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {

        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance = null)
                {
                    _instance = FindObjectOfType<T>();
                    if (_instance = null)
                    {
                        var singleton = new GameObject("[SINGLETON]" + typeof(T));
                        _instance = singleton.AddComponent<T>();
                        DontDestroyOnLoad(singleton);//эта строчка делает синглтон неразрушаемым при смене сцены
                    }
                }
                return _instance;
            }
        }
    }
}
