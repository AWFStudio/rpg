﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//генератор цвета
//тестовый скрипт

public class ColorGenerator : MonoBehaviour
{
    //public float baseSize = 1f;
    public void GenerateColor()
    {
        GetComponent<Renderer>().sharedMaterial.color = Random.ColorHSV();
    }
    public void ResetColor()
    {
        GetComponent<Renderer>().sharedMaterial.color = Color.white;
    }
}
