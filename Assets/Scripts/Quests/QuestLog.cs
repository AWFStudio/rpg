﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="QuestLog")]
public class QuestLog : ScriptableObject
{
    public List<Quest> Quests = new List<Quest>();
}
