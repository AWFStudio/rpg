﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//единичный квест (любой)

public class Quest : ScriptableObject
{
    [SerializeField]
    private string questName;//название квеста

    [SerializeField]
    private bool isFinished;//стадия

    public bool IsFinished { get => isFinished; set => isFinished = value; }
    public string QuestName { get => questName; set => questName = value; }
}
