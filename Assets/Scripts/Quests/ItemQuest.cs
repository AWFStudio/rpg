﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//квест на получение итема

[Serializable]
public class ItemQuest : Quest
{
    [SerializeField]
    private string item;//что за итем

    public string Item { get => item; set => item = value; }

    public ItemQuest()//это вместо конструктора, который не используется в скриптуемых объектах
    {
        item = "Default";
    }
}
