﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//квест на убийство мобов

[Serializable]
public class HuntQuest : Quest
{
    [SerializeField]
    private string enemy;//каких мобов убиваем
    [SerializeField]
    private int currentCount;//сколько уже убили
    [SerializeField]
    private int maxCount;//сколько нужно убить
    public string Enemy { get => enemy; set => enemy = value; }
    public int CurrentCount { get => currentCount; set => currentCount = value; }
    public int MaxCount { get => maxCount; set => maxCount = value; }

    public HuntQuest()//это вместо конструктора, который не используется в скриптуемых объектах
    {
        enemy = "Default";
        currentCount = 0;
        maxCount = 10;
    }
}
