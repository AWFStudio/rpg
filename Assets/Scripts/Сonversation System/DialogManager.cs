﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

//этот класс пользуется классом DialogSettings наравне с классом Messages
//служит для вывода диалогов, т.е. таких сообщений, где требуется ответ игрока
//висит на панели диалогов


public class DialogManager : MonoBehaviour
{
    public TextAsset asset;//подгружаемый xml файл

    XMLSettings dialog;//диалог
    GroupManager group;

    //кнопки
    public GameObject[] answers;
    public GameObject answerButtonPrefab;
    public GameObject nextButton;

    public Text NPCtext;//область вывода реплик собеседника
    public GameObject answersPanel;//область вывода реплик игрока
    public GameObject dialogPanel;//панель диалога

    public GameObject NPCAvatarPanel;//панель аватара собеседника
    public GameObject PlayerAvatarPanel;//панель аватара игрока

    GameObject selectedMemberAvatar;//аватар персонажа, который инициировал диалог
    public string fileName;
    public GameObject InterlocutorAvatar;

    public int id = 0;//идентификатор реплики, с которой будет начинаться диалог

    public void UpdateDialog()
    {
        group = FindObjectOfType<GroupManager>();
        //подгружаем текст, в зависимости от выбранного языка
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Dialogs/" + fileName);

        dialog = XMLSettings.Load(asset);//выгружаем диалог

        //отображаем аватар персонажа, который инициировал диалог
        for (int i = 0; i < group.groupMembersCount; i++)
        {
            if (group.members[i].GetComponent<CharacterProfile>().InFocus)
            {
                selectedMemberAvatar = Instantiate(group.members[i].GetComponent<CharacterProfile>().CharacterPict) as GameObject;
                selectedMemberAvatar.transform.SetParent(PlayerAvatarPanel.transform, false);
            }
        }
        //отображаем аватар непися-собеседника
        InterlocutorAvatar = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Character Pictures/" + fileName));
        InterlocutorAvatar.transform.SetParent(NPCAvatarPanel.transform, false);
        ShowDialog();
    }


    //метод перехода к следующему сообщению диалога
    public void Next(int nextNode, bool endDialog)
    {
        if (dialog.nodes[id].endnode == "true")//если реплика не требует ответа
        {
            id++; //увеличиваем идентификатор на единицу, чтобы переместиться к следующей реплике
            for (int j = 0; j < answers.Length; j++)//чистим поле ответов
            {
                Destroy(answers[j]);
            }
            ShowDialog();//обновляем диалог
        }
        else//иначе проверяем, не заканчивается ли здесь диалог
        {
            if (endDialog != true)//если нет
            {
                id = nextNode;//то осуществляем переход к указанной реплике
                for (int j = 0; j < answers.Length; j++)//чистим поле ответов
                {
                    Destroy(answers[j]);
                }
                ShowDialog();//обновляем диалог
            }
            else//а если да
            {
                for (int j = 0; j < answers.Length; j++)//чистим поле ответов
                {
                    Destroy(answers[j]);
                    Destroy(InterlocutorAvatar);
                }

                UIManager.dialogPanelIsActive = false;//помечаем окно, как закрытое
                dialogPanel.SetActive(false);//и закрываем его
                                             //здесь делаем необходимую запись в журнал
            }
        }
    }


    //метод для навешивания на кнопку "далее"
    //потому что ивентсистем не хочет обрабатывать событие онклик, если у метода, связанного с ним, больше одного атрибута
    public void NextButton()
    {
        Next(0, false);
    }

    public void ShowDialog()
    {

        NPCtext.text = dialog.nodes[id].text;//выгружаем нужный текст

        if (dialog.nodes[id].endnode != "true")//если реплика непися предусматривает реплики игрока на выбор
        {
            nextButton.SetActive(false);//выключаем кнопку "далее"

            if (answers.Length < dialog.nodes[id].answers.Length)//проверяем, существует ли массив ответов, и если нет... 
            {
                answers = new GameObject[dialog.nodes[id].answers.Length];//то создаем его
            }

            for (int j = 0; j < dialog.nodes[id].answers.Length; j++)//и для каждого варианта ответа
            {
                answers[j] = Instantiate(answerButtonPrefab) as GameObject;//создаем кнопку
                answers[j].transform.SetParent(answersPanel.transform, false);//устанавливаем для нее родительский элемент
                answers[j].GetComponentInChildren<Text>().text = dialog.nodes[id].answers[j].anstext;//назначаем текст кнопке
                answers[j].GetComponent<DialogButtonsManager>().reaction = dialog.nodes[id].answers[j].tonode;//к какой реплике непися нужно перейти после выбора этого варианта ответа
                if (dialog.nodes[id].answers[j].end == "true")//если нажатие на эту кнопку заканчивает диалог
                {
                    answers[j].GetComponent<DialogButtonsManager>().endDialog = true;//то сообщаем это менеджеру кнопки
                }
                if (dialog.nodes[id].answers[j].addMember)//если по нажатию этой кнопки собеседник добавляется в группу
                {
                    answers[j].GetComponent<DialogButtonsManager>().memberToAdd = dialog.nodes[id].NPCID;
                    answers[j].GetComponent<DialogButtonsManager>().addMember = true;
                }
            }
        }
        else
        {
            nextButton.SetActive(true);//включаем кнопку "далее"
        }
    }

}
