﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//этот класс пользуется классом DialogSettings наравне с классом Dialog
//служит для вывода сообщений (не диалогов), т.е. таких, которые не ждут ответа от игрока
//висит на панели сообщений
public class MessageManager : MonoBehaviour
{

    public TextAsset asset;//подгружаемый xml файл

    XMLSettings message;//сообщение

    public Text text;//область вывода текста

    public int id;//идентификатор сообщения в массиве

    EffectsManager EffectsManager;
    void Start()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/Messages");
        message = XMLSettings.Load(asset);//выгружаем сообщение
        text.text = message.Messages[id].text;//выгружаем нужный бокс
        EffectsManager = FindObjectOfType<EffectsManager>();
    }

    //этот метод описывает реакцию на закрытие окна сообщения (нажание кнопки "ок"), в зависимости от id сообщения
    public void EndMessage()
    {
        UIManager.messagePanelIsActive = false;

        if (id == 0 || id == 1)
        {
            EffectsManager.AddEffect(EffectsList.Effects[0]);
        }
    }

}
