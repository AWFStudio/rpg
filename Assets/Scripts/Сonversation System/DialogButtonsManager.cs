﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;//искин для работы навмеша


//этот скрипт управляет кнопками в диалогах и сообщениях
//висит на кнопке варианта ответа

public class DialogButtonsManager : MonoBehaviour
{

    public int reaction;//идентификатор реплики, к которой следует осуществить переход
    public bool endDialog;//конец диалога
    public DialogManager dialog;//ссылки на скрипты диалога

    public bool addMember = false;
    public int memberToAdd;

    UIManager UI;
    GroupManager group;

    public GameObject[] potentialMembers;

    void Start()
    {
        group = FindObjectOfType<GroupManager>();
        UI = FindObjectOfType<UIManager>();
        dialog = GetComponentInParent<DialogManager>();
    }

    public void Next()//метод для навешивания на префаб кнопки выбора варианта ответа
    {
        dialog.Next(reaction, endDialog);
        if (addMember)
        {
            AddMember(memberToAdd);
        }
    }

    void AddMember(int memberToAdd)//когда добавляем в группу нового персонажа
    {
        potentialMembers = GameObject.FindGameObjectsWithTag("Potential member");//находим на сцене всех потенциальных мемберов
        foreach (GameObject potentialMember in potentialMembers)//перебираем их
        {
            if (potentialMember.GetComponent<CharacterProfile>().CharacterID == memberToAdd)//если id мембера совпадает с id переданным в метод
            {
                potentialMember.GetComponent<CharacterProfile>().DialogEnable = false;//отключаем возможность завязать с ним диалог
                potentialMember.tag = "Member";//присваиваем ему тег 
                NavMeshAgent navMeshAgent = potentialMember.AddComponent<NavMeshAgent>();//навешиваем на него навмешагент
                group.groupMembersCount++;//увеличиваем счетчик членов группы
                group.members[group.groupMembersCount - 1] = potentialMember;//помещаем его в массив группы
                UI.AddMemberInfo();//обновляем интерфейс группы
            }
        }

    }
}
