﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//это локализатор основного интерфейса
//висит на гейм-контроллере

public class UILocalizator : MonoBehaviour
{
    TextAsset UIAsset;
    XMLSettings UI;

    [Header("панель паузы")]
    [SerializeField]
    Text returnBttn;
    [SerializeField]
    Text saveBttn;
    [SerializeField]
    Text loadBttn;
    [SerializeField]
    Text optionsBttn;
    [SerializeField]
    Text exitBttn;

    [Header("панель предупреждения (вы точно хотите выйти?)")]
    [SerializeField]
    Text warningText;
    [SerializeField]
    Text exitToMenuBttn;
    [SerializeField]
    Text gameExitBttn;
    [SerializeField]
    Text cancelBttn;

    [Header("панель диалога")]
    [SerializeField]
    Text nextBttn;

    [Header("кнопки главной служебной панели")]
    [SerializeField]
    Text equipmentBttn;
    [SerializeField]
    Text characterBttn;
    [SerializeField]
    Text skillsBttn;
    [SerializeField]
    Text journalBttn;
    [SerializeField]
    Text mapBttn;

    [Header("заголовки главной служебной панели")]
    [SerializeField]
    Text inventoryTitle;

    void Start()
    {
        ApplyLanguageSettings();
    }

    //локализация элементов UI
    public void ApplyLanguageSettings()
    {
        UIAsset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Main UI");
        UI = XMLSettings.Load(UIAsset);

        //меню паузы
        returnBttn.text = UI.UIelements[0].text;
        saveBttn.text = UI.UIelements[1].text;
        loadBttn.text = UI.UIelements[2].text;
        optionsBttn.text = UI.UIelements[3].text;
        exitBttn.text = UI.UIelements[4].text;

        //панель предупреждения (вы точно хотите выйти?)
        warningText.text = UI.UIelements[5].text;
        exitToMenuBttn.text = UI.UIelements[6].text;
        gameExitBttn.text = UI.UIelements[7].text;
        cancelBttn.text = UI.UIelements[8].text;

        //панель диалога
        nextBttn.text = UI.UIelements[9].text;

        //кнопки главной служебной панели
        equipmentBttn.text = UI.UIelements[10].text;
        characterBttn.text = UI.UIelements[11].text;
        skillsBttn.text = UI.UIelements[12].text;
        journalBttn.text = UI.UIelements[13].text;
        mapBttn.text = UI.UIelements[14].text;

        //заголовки
        inventoryTitle.text = UI.UIelements[17].text;
    }



}
