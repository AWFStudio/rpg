﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;//для чтения XML файлов
using System.IO;//для работы со службами файлового ввода/вывода

//этот скрипт ни на чем не висит

[XmlRoot("root")]//указываем корень XML файла
public class XMLSettings //убираем монобихевиор
{
    //отождествляем бокс с элементом соответствующего списка:

    //для диалогов
    [XmlElement("node")]
    public Node[] nodes;

    //для элементов UI
    [XmlElement("UIelement")]
    public UIelement[] UIelements;

    //для сообщений
    [XmlElement("message")]
    public Message[] Messages;

    //для лейблов персонажей
    [XmlElement("label")]
    public CharacterLabel[] CharacterLabels;

    //для итемов
    [XmlElement("item")]
    public InventoryItem[] InventoryItems;

    public static XMLSettings Load(TextAsset _xml)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(XMLSettings));//создаем экземпляр сериалайзера
        StringReader reader = new StringReader(_xml.text);//создаём ридер, чтобы выводить только содержимое тегов - текст, а сами теги не выводить
        XMLSettings settings = serializer.Deserialize(reader) as XMLSettings;//десериализуем xml
        return settings;
    }
}

[System.Serializable]//сериализуем
public class Node//создаем новый класс 
{
    [XmlAttribute("NPCID")]
    public int NPCID;

    [XmlElement("text")]
    public string text;

    [XmlElement("endnode")]
    public string endnode;

    [XmlArray("answers")]
    [XmlArrayItem("answer")]
    public Answer[] answers;

}

[System.Serializable]
public class Answer
{
    [XmlAttribute("tonode")]
    public int tonode;

    [XmlAttribute("addmember")]
    public bool addMember;

    [XmlElement("anstext")]
    public string anstext;

    [XmlElement("end")]
    public string end;
}

[System.Serializable]
public class UIelement
{
    [XmlAttribute("ElementKey")]
    public int ElementKey;

    [XmlElement("text")]
    public string text;

    [XmlElement("headline")]
    public string headline;

    [XmlElement("mod_01")]
    public string mod_01;
    
    [XmlElement("mod_02")]
    public string mod_02;
    
    [XmlElement("mod_03")]
    public string mod_03;

    [XmlElement("mod_04")]
    public string mod_04;

    [XmlElement("mod_05")]
    public string mod_05;

    [XmlElement("mod_06")]
    public string mod_06;
}

[System.Serializable]
public class Message
{
    [XmlElement("text")]
    public string text;

    [XmlAttribute("id")]
    public int id;
}

[System.Serializable]
public class CharacterLabel
{
    [XmlAttribute("characterID")]
    public int characterID;

    [XmlElement("text")]
    public string text;

}

[System.Serializable]
public class InventoryItem
{
    [XmlElement("name")]
    public string name;

    [XmlElement("type")]
    public string type;

    [XmlElement("description")]
    public string description;
}




