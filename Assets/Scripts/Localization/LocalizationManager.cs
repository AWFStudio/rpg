﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
//скрипт должен висеть на менеджере сцены

public class LocalizationManager : MonoBehaviour
{
    public static string currentLanguage = "ru_RU";//текущий язык
    void Awake()
    {
        if (!PlayerPrefs.HasKey("Language"))//если игрок еще не установил предпочитаемый язык
        {
            //смотрим, какой язык используется системой приложения игрока
            if (Application.systemLanguage == SystemLanguage.Russian || Application.systemLanguage == SystemLanguage.Ukrainian || Application.systemLanguage == SystemLanguage.Belarusian)
            {
                //если это русский, украинский или беларусский, то создаем в PlayerPrefs новую строку Language и присваиваем ей значение ru_RU
                //т.е. устанавливаем Русский язык 
                PlayerPrefs.SetString("Language", "ru_RU");
            }
            else
            {
                //иначе устанавливаем английский
                PlayerPrefs.SetString("Language", "en_US");
            }
        }
        currentLanguage = PlayerPrefs.GetString("Language");//берем текущий язык из настроек игрока
    }

    public string CurrentLanguage//свойство, чтобы получить доступ к приватной переменной currentLanguage
    {
        get
        {
            return currentLanguage;//указываем, что значение переменной currentLanguage...
        }
        set
        {
            //будем брать из строки Language настроек игрока 
            PlayerPrefs.SetString("Language", value);
            currentLanguage = PlayerPrefs.GetString("Language");
        }
    }


}
