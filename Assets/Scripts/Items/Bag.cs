﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Bag : Item
{
    [SerializeField]
    private int bagCellsAmount;//количество ячеек
    public int BagCellsAmount { get => bagCellsAmount; set => bagCellsAmount = value; }
    public Bag()//это вместо конструктора, который не используется в скриптуемых объектах
    {
        ItemAppointment = Item_Appointment.bag;
        MaxCount = 1;
        InCell = 1;
    }
}
