﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//любой айтем

public class Item : ScriptableObject
{
    public enum Item_Appointment
    {
        bag,
        armor,
        weapon,
        shield,
        accessory,
        consumable,
        ingredient,
        special
    }

    [SerializeField]
    private int itemID;//ID предмета
    [SerializeField]
    private string itemName;//название предмета
    [SerializeField]
    private string itemDescription;//описание предмета
    [SerializeField]
    private int itemCost;//стоимость 
    [SerializeField]
    private int maxCount;//сколько таких итемов можно сложить в одну ячейку 
    [SerializeField]
    private int inCell;//сколько предметов этого типа лежит сейчас в ячейке инвентаря 
    [SerializeField]
    private float itemWeight;//вес
    [SerializeField]
    private Sprite itemIcon;//иконка предмета
    [SerializeField]
    private Item_Appointment itemAppointment;//тип предмета

    public string ItemName { get => itemName; set => itemName = value; }
    public string ItemDescription { get => itemDescription; set => itemDescription = value; }
    public int ItemID { get => itemID; set => itemID = value; }
    public int ItemCost { get => itemCost; set => itemCost = value; }
    public int MaxCount { get => maxCount; set => maxCount = value; }
    public int InCell { get => inCell; set => inCell = value; }
    public float ItemWeight { get => itemWeight; set => itemWeight = value; }
    public Sprite ItemIcon { get => itemIcon; set => itemIcon = value; }
    public Item_Appointment ItemAppointment { get => itemAppointment; set => itemAppointment = value; }
}
