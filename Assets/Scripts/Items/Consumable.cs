﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Consumable : Item
{

    public enum Consumable_Type
    {

    }
    [SerializeField]
    private Consumable_Type consumableType;//тип расходника
    public Consumable_Type ConsumableType { get => consumableType; set => consumableType = value; }

    public Consumable()//это вместо конструктора, который не используется в скриптуемых объектах
    {
        ItemAppointment = Item_Appointment.consumable;
        MaxCount = 99;
        InCell = 1;
    }
}
