﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Items")]

public class ItemsList : ScriptableObject
{
    public List<Item> Items = new List<Item>();

}



