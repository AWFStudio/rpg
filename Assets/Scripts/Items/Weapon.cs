﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Weapon : Item
{
    public enum Weapon_Type
    {
        twoHanded,
        rightHanded,
        leftHanded,
        bothHanded
    }

    public enum Weapon_Class
    {
        crossbow,
        longSword,
        sword
    }

    public enum Weapon_Condition
    {
        rusted,
        common,
        solid,
        unique
    }

    [SerializeField]
    private Weapon_Type weaponType;//тип оружия
    [SerializeField]
    private Weapon_Class weaponClass;//класс оружия
    [SerializeField]
    private Weapon_Condition weaponCondition;//состояние оружия
    [SerializeField]
    private int weaponLevel;//уровень оружия
    [SerializeField]
    private int minDamage;//минимальный урон оружия
    [SerializeField]
    private int maxDamage;//максимальный урон оружия
    [SerializeField]
    private float range;//дальность
    [SerializeField]
    private int chanceToBreak;//шанс, что предмет сломается при атаке


    public Weapon_Type WeaponType { get => weaponType; set => weaponType = value; }
    public Weapon_Class WeaponClass { get => weaponClass; set => weaponClass = value; }
    public int MinDamage { get => minDamage; set => minDamage = value; }
    public int MaxDamage { get => maxDamage; set => maxDamage = value; }
    public int WeaponLevel { get => weaponLevel; set => weaponLevel = value; }
    public Weapon_Condition WeaponCondition { get => weaponCondition; set => weaponCondition = value; }
    public float Range { get => range; set => range = value; }
    public int ChanceToBreak { get => chanceToBreak; set => chanceToBreak = value; }

    public Weapon()//это вместо конструктора, который не используется в скриптуемых объектах
    {
        ItemAppointment = Item_Appointment.weapon;
        MaxCount = 1;
        InCell = 1;

        if (weaponClass == Weapon_Class.sword)
        {
            Range = 1.2f;
        }
        if (weaponClass == Weapon_Class.longSword)
        {
            Range = 1.5f;
        }
    }
}
