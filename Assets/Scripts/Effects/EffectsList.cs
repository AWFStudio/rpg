﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ThePlace;


//все эффекты

public class EffectsList : Singleton<EffectsList>
{

    public static GameObject[] Effects = new GameObject[]
    {
        //wounded (0)
        Resources.Load<GameObject>("Prefabs/UIPrefabs/Effects/Wounded"),

        //frozen (1)
        Resources.Load<GameObject>("Prefabs/UIPrefabs/Effects/Frozen"),

        //burning (2)
        Resources.Load<GameObject>("Prefabs/UIPrefabs/Effects/Burning"),

        //bleeding (3)
        Resources.Load<GameObject>("Prefabs/UIPrefabs/Effects/Bleeding")
                     
    };

    }

    //ранение (уменьшает все показатели на х%)

    //кровотечение (наносит урон в размере х% от максимального количества ОЗ каждый ход)

    //горение (наносит урон в размере х% от максимального количества ОЗ каждый ход)

    //холод (уменьшает ловкость на х%)

    //обморожение (урон, как результат длительного воздействия холода)

    //скольжение (вероятность упасть х%, применение любого умения повышает вероятность еще на х%)

    //мокрый (уменьшает шанс загореться на х%, снимает горение)

    //промаслен (увеличивает урон от горения на х%)

    //отравление (наносит урон в размере х% от максимального количества ОЗ каждый ход)

    //слепота (уменьшает ловкость на х%)

    //заикание (понижает шанс успешного применения вербального умения)

    //немота (невозможно использовать вербальные умения)

    //сон (невозможно совершать действия)

    //испуг (невозможно совершать действия)

    //оглушение (невозможно совершать действия)

    //паралич (невозможно совершать действия)

    //падение (невозможно совершать действия)

    //слабость (снижает показатель силы на х%)

    //опьянение (снижает показатели ловкости и интеллекта на х%)

    //похмелье (снижает показатели ловкости и интеллекта на х%)

    //отравление (наносит урон в размере х% от максимального количества ОЗ каждый ход)

    //инфекция (наносит урон в размере х% от максимального количества ОЗ каждый ход, шанс х% заразить ближайшего союзника тем же самым)

    //вонь (снижает показатель харизмы на х%) (учитывается при общении с другими персонажами)

    //голод (уменьшает все показатели на х%)

    //жажда (уменьшает все показатели на х%)

    //ломка (уменьшает все показатели на х%)

    //под наркотой (индивидуальный эффект)

    //депрессия (уменьшает все показатели на х%)

    //усталость (уменьшает все показатели на х%)

    //недосып (уменьшает все показатели на х%)

    //переедание (?)

    //очарование (персонаж лупит своих)

    //безумие (персонаж лупит всех подряд: как своих, так и чужих)

    //переваривание (когда персонаж оказывается внутри некоторых монстров) (наносит урон в размере х% от максимального количества ОЗ каждый ход, невозможно совершать действия)

    //удушение (Персонаж не может дышать. Через несколько ходов он потеряет сознание, ещё через несколько начнёт получать перманентные штрафы к интеллекту) (хардкор-мод)

    //окаменение (невозможно совершать действия)

    //замедление (все действия требуют на х% больше очков действй)

    //превращение (персонаж превращается в другое сщество)

    //защита (уменьшает входящий урон на х%)

    //ярость (увеличивает шанс крита на х%)

    //азарт боя (увеличивает количество очков действий)

    //укрытие (уменьшает шанс быть атакованным на х%, уменьшает ловкость противника на х%)

    //невидимость

    //блокирование (уменьшает входящий урон на х%)

    //отражение урона (уменьшает входящий урон на х% и возвращает х% от этого количества обратно атаковавшему)

    //контратака (срабатывает с шансом х при успешном блокировании) (возвращает х% от заблокированного урона обратно атаковавшему)

    //дерзость (повышает шанс стать объектом следующей атаки на х%)

    //регенерация (увеличивает скорость восстановления оз на х%)

    //вампиризм (увеличивает оз персонажа на х% от урона, нанесенного персонажем противнику)

    //астральный вампиризм (когда наносим урон противнику, крадем % от его запаса маны)

    //полет (возможность пересекать некоторые препятствия и игнорировать некоторые эффекты)

    //хождение по воде (?)

    //обнаружение невидимости

    //нематериальность (защита от физ.атак)


