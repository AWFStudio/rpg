﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//скрипт эффекта
//висит на каждом префабе иконки эффекта

public class Effect : MonoBehaviour
{
    private string effectName;

    private string effectDescription;

    private bool isBuff;//бафф = 1, дебафф = 0

    public string EffectName { get => effectName; set => effectName = value; }
    public string EffectDescription { get => effectDescription; set => effectDescription = value; }
    public bool IsBuff { get => isBuff; set => isBuff = value; }
}
