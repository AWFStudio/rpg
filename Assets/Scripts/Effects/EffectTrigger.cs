﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//триггер эффекта
//висит на объекте, при входе в который, на персонажа вешается эффект
//когда персонаж заходит в область применения эффекта, то в панели эффектов отображается соответствующая иконка
//области применения эффекта должны обязательно иметь твердое тело и коллайдер-триггер

public class EffectTrigger : MonoBehaviour
{
   CharacterProfile character;
   GameObject charactersPanel;

    public GameObject effect;
    GroupManager group;

    void OnTriggerEnter(Collider other)
    {
        group = FindObjectOfType<GroupManager>();
        if (other.tag == "Member")
        {
            character = other.GetComponent<CharacterProfile>();
            for (int i = 0; i < group.groupMembersCount; i++)
            {
                if (group.members[i].GetComponent<CharacterProfile>().CharacterID == character.CharacterID)
                {
                    charactersPanel = UIManager.characterPanels[i];
                    break;
                }
            }
            charactersPanel.GetComponent<EffectsManager>().AddEffect(effect);
        }
    }

}
