﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Основной скрипт эффектов
//Висит на панели мембера

public class EffectsManager : MonoBehaviour
{
    public GameObject effectsPanel;//панель иконок эффектов

    public Effect[] effects;

    GameObject effectIcon;//иконка эффекта

    public int effectsCount;

    private void Start()
    {
        effects = new Effect[20];
    }

    //добавляем эффект (пока только иконку)
    public bool AddEffect(GameObject effect)
    {
        effectsCount += 1;

        for (int i = 0; i < effectsCount; i++)//перебираем ячейки, и...
        {
            if (effects[i] == null)//если ячейка массива свободна
            {
                effectIcon = Instantiate(effect) as GameObject;//инстанциируем иконку эффекта
                effectIcon.transform.SetParent(effectsPanel.transform, false);//устанавливаем родительский элемент
                effects[i] = effect.GetComponent<Effect>();//записываем в массив
                return true;//прерываем цикл
            }
        }
        return false;
    }
}
