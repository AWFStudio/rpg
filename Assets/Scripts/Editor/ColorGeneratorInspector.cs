﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ColorGenerator))]
public class ColorGeneratorInspector : Editor
{
    public override void OnInspectorGUI()
    {
        ColorGenerator currentObject = (ColorGenerator)target;

        GUILayout.Label("Заголовок");//заголовок

        ////создание слайдера, изменяющего размер
        //currentObject.baseSize = EditorGUILayout.Slider("Размер", currentObject.baseSize, 0.1f, 2f);
        //currentObject.transform.localScale = Vector3.one * currentObject.baseSize;

        GUILayout.BeginHorizontal();//элементы, расположенные внутри этих "тегов" будут располагаться рядом по горизонтали

        //создание кнопок, рандомно меняющих цвет и возвращающих белый
        if(GUILayout.Button("Generate color"))
        {
            currentObject.GenerateColor();
        }
        if(GUILayout.Button("Default color"))
        {
            currentObject.ResetColor();
        }
        GUILayout.EndHorizontal();
    }

}
