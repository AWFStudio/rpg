﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Weapon))]

public class WeaponEditor : Editor
{
    Weapon currentWeapon;

    public override void OnInspectorGUI()//Внутри этой функции можно добавлять собственный графический интерфейс для инспектора
    {
        currentWeapon = (Weapon)target;//оружие, которое сейчас редактируется инспектором

        currentWeapon.WeaponClass = (Weapon.Weapon_Class)EditorGUILayout.EnumPopup("Weapon class", currentWeapon.WeaponClass);

        currentWeapon.WeaponCondition = (Weapon.Weapon_Condition)EditorGUILayout.EnumPopup("Weapon condition", currentWeapon.WeaponCondition);

        string newItemName = EditorGUILayout.TextField("Item name", currentWeapon.ItemName);
        currentWeapon.ItemName = newItemName;

        int newItemID = EditorGUILayout.IntField("Item ID", currentWeapon.ItemID);
        currentWeapon.ItemID = newItemID;
        
        int newWeaponLevel = EditorGUILayout.IntField("Weapon level", currentWeapon.WeaponLevel);
        currentWeapon.WeaponLevel = newWeaponLevel;
        
        int newMinDamage = EditorGUILayout.IntField("Min damage", currentWeapon.MinDamage);
        currentWeapon.MinDamage = newMinDamage;

        int newMaxDamage = EditorGUILayout.IntField("Max damage", currentWeapon.MaxDamage);
        currentWeapon.MaxDamage = newMaxDamage;

        int newItemCost = EditorGUILayout.IntField("Item cost", currentWeapon.ItemCost);
        currentWeapon.ItemCost = newItemCost;

        float newItemRange = EditorGUILayout.FloatField("Item range", currentWeapon.Range);
        currentWeapon.Range = newItemRange;


        currentWeapon.ItemIcon = (Sprite)EditorGUILayout.ObjectField(currentWeapon.ItemIcon, typeof(Sprite), false);

        if (currentWeapon.WeaponClass == Weapon.Weapon_Class.sword)
        {
            currentWeapon.WeaponType = Weapon.Weapon_Type.bothHanded;
            currentWeapon.ItemWeight = 2;
        }
        else if (currentWeapon.WeaponClass == Weapon.Weapon_Class.longSword)
        {
            currentWeapon.WeaponType = Weapon.Weapon_Type.twoHanded;
            currentWeapon.ItemWeight = 3;
        }
        else if (currentWeapon.WeaponClass == Weapon.Weapon_Class.crossbow)
        {
            currentWeapon.WeaponType = Weapon.Weapon_Type.twoHanded;
        }

        if (currentWeapon.WeaponCondition == Weapon.Weapon_Condition.rusted)
        {
            int newChanceToBreak = EditorGUILayout.IntField("Chance to break", currentWeapon.ChanceToBreak);
            currentWeapon.ChanceToBreak = newChanceToBreak;
        }
    }
}
