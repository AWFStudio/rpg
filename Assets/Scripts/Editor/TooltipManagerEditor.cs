﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TooltipManager))]

public class TooltipManagerEditor : Editor
{
    TooltipManager currentTooltip;

    public override void OnInspectorGUI()//Внутри этой функции можно добавлять собственный графический интерфейс для инспектора
    {
        currentTooltip = (TooltipManager)target;

        currentTooltip.TooltipType = ((TooltipManager.TypeOfTooltip)EditorGUILayout.EnumPopup("Tooltip Type", currentTooltip.TooltipType));

        if (currentTooltip.TooltipType == TooltipManager.TypeOfTooltip.text)
        {
            int newUIElementID = EditorGUILayout.IntField("UI element ID", currentTooltip.UIelementID);
            currentTooltip.UIelementID = newUIElementID;
        }
    }

}
