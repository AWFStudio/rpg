﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(QuestLog))]//указываем, инспектор какого скрипта будем менять
public class QuestLogEditor : Editor//наследуем от класса "эдитор"
{
    private QuestLog questLog;

    //Модификатор override требуется для расширения или изменения реализации унаследованного метода
    public override void OnInspectorGUI()//Внутри этой функции можно добавлять собственный графический интерфейс для инспектора
    {
        questLog = (QuestLog)target;//ссылка на отображаемый в инспекторе скрипт
        SerializedObject serializedObject = new SerializedObject(questLog);//сериализуем нужный скрипт
        serializedObject.Update();//и обновляем его

        SerializedProperty serializedPropertyList = serializedObject.FindProperty("Quests");//указываем сериализуемые свойства (это будет все содержимое массива Quests)
        Draw(serializedPropertyList);//отрисовываем (выводим в инспекторе) весь список сериализумых свойств
    }

    //метод отображения элементов в инспекторе
    private void Draw(SerializedProperty list)
    {
        EditorGUILayout.PropertyField(list);//создаем поле

        if (list.isExpanded)//если список помечен как отображаемый инспектором
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));//найти все его элементы
            ShowElements(list, false);//показать все элементы списка, но не показывать их названия ("элемент 1", "элемент 2" и т.д.)
        }

        //кнопка добавления квеста на убийство мобов
        if (GUILayout.Button("Add Hunt Quest"))//если нажимаем
        {
            HuntQuest huntQuest = ScriptableObject.CreateInstance<HuntQuest>();//то создаем новый квест
            huntQuest.name = "New huntQuest";
            questLog.Quests.Add(huntQuest);//и добавляем его в массив квестов
            AssetDatabase.AddObjectToAsset(huntQuest, questLog);//добавляем созданный квест в ассет
            AssetDatabase.SaveAssets();//сохраняем ассет

            if (!list.isExpanded)//если список помечен как не отображаемый
            {
                list.isExpanded = true;//то помечаем его, как отображаемый
            }
        }

    }

    private void ShowElements(SerializedProperty list, bool showElementLabels = true)//отобразить элементы (квесты) и их названия
    {
        for (int i = 0; i < list.arraySize; i++)//для каждого элемента списка (квеста)
        {
            EditorGUILayout.BeginHorizontal();//заключаем следующий код в этот контейнер, чтобы кнопка удаления элемента выводилась справа от него, а не под ним
            if (showElementLabels)//если опция "показывать названия элементов" включена
            {
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));//показываем названия элементов
            }
            else//иначе
            {
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), GUIContent.none);//скрываем названия элементов
            }
            //добавляем кнопку, чтобы удалять ненужные квесты
            if (GUILayout.Button("Delete", EditorStyles.miniButtonRight, GUILayout.ExpandWidth(false)))//создаем кнопку, указываем ей стиль, запрещаем растягивать ее по горизонтали
            {
                //если она нажата
                Object.DestroyImmediate(list.GetArrayElementAtIndex(i).objectReferenceValue, true);//удаляем квест из ассетов
                questLog.Quests.RemoveAt(i);//и затем удаляем его из инспектора
                AssetDatabase.SaveAssets();//сохраняем ассет
            }
            EditorGUILayout.EndHorizontal();//закрываем контейнер с выравниванием

        }
    }
}
