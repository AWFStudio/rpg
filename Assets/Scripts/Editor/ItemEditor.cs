﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;
//using UnityEditor.SceneManagement;

////этот скрипт должен лежать в папке Editor, чтобы не учитываться при билде игры

////[CustomEditor(typeof(...))]
//public class ItemEditor : Editor
//{
//    Item currentItem;

//    public override void OnInspectorGUI()
//    {
//        currentItem = (Item)target;//скрипт, который сейчас редактируется инспектором
//        SerializedObject serializedObject = new SerializedObject(currentItem);//сериализуем нужный скрипт
//        serializedObject.Update();//и обновляем его

//        //идентификатор (для локализации)
//        int newItemID = EditorGUILayout.IntField("Item ID", currentItem.ItemID);
//        currentItem.ItemID = newItemID;

//        //картинка
//        currentItem.ItemIcon = (Sprite)EditorGUILayout.ObjectField(currentItem.ItemIcon, typeof(Sprite), false);

//        //название
//        string newItemName = EditorGUILayout.TextField("Item name", currentItem.ItemName);
//        currentItem.ItemName = newItemName;

//        //описание
//        string newItemDescription = EditorGUILayout.TextField("Item Description", currentItem.ItemDescription);
//        currentItem.ItemDescription = newItemDescription;

//        //стоимость
//        int newItemCost = EditorGUILayout.IntField("Item cost", currentItem.ItemCost);
//        currentItem.ItemCost = newItemCost;

//        //вес
//        float newItemWeight = EditorGUILayout.FloatField("Item weight", currentItem.ItemWeight);
//        currentItem.ItemWeight = newItemWeight;

//        //сколько таких итемов можно положить в одну ячейку
//        int newItemMaxCount = EditorGUILayout.IntField("Maximum in cell", currentItem.MaxCount);
//        currentItem.MaxCount = newItemMaxCount;

//        //сколько таких итемов уже лежит в ячейке
//        int newItemInCell = EditorGUILayout.IntField("In cell", currentItem.InCell);
//        currentItem.InCell = newItemInCell;

//        //назначение итема (оружие, сумка и т.д.)
//        currentItem.ItemAppointment = (Item.Item_Appointment)EditorGUILayout.EnumPopup("Appointment", currentItem.ItemAppointment);

//        //если это оружие
//        if (currentItem.ItemAppointment == Item.Item_Appointment.weapon)
//        {
//            //класс оружия (меч, лук и т.д.)
//            currentItem.WeaponClass = (Item.Weapon_Class)EditorGUILayout.EnumPopup("Weapon class", currentItem.WeaponClass);

//            //минимальный урон
//            int newMinDamage = EditorGUILayout.IntField("Minimum damage", currentItem.MinDamage);
//            currentItem.MinDamage = newMinDamage;

//            //максимальный урон
//            int newMaxDamage = EditorGUILayout.IntField("Maximum damage", currentItem.MaxDamage);
//            currentItem.MaxDamage = newMaxDamage;
//        }

//        //если это расходник
//        if (currentItem.ItemAppointment == Item.Item_Appointment.consumable)
//        {
//            //тип расходника
//            currentItem.ConsumableType = (Item.Consumable_Type)EditorGUILayout.EnumPopup("Consumable type", currentItem.ConsumableType);
//        }

//        //если это сумка
//        if (currentItem.ItemAppointment == Item.Item_Appointment.bag)
//        {
//            //количество ячеек в ней
//            int newbagCellsAmount = EditorGUILayout.IntField("Amount of cells", currentItem.BagCellsAmount);
//            currentItem.BagCellsAmount = newbagCellsAmount;
//        }

//        AssetDatabase.SaveAssets();//сохраняем ассет
//    }
//}

