﻿using UnityEditor;

//этот скрипт должен лежать в папке Editor, чтобы не учитываться при билде игры

[CustomEditor(typeof(ItemCell))]
public class ItemCellInspector : Editor
{
    public override void OnInspectorGUI()
    {
        ItemCell currentCell = (ItemCell)target;//скрипт, который сейчас редактируется инспектором

        currentCell.cellType = (ItemCell.CellType)EditorGUILayout.EnumPopup("Cell type", currentCell.cellType);

        if (currentCell.cellType == ItemCell.CellType.outfit)
        {
            currentCell.outfitCellType = (ItemCell.OutfitCellType)EditorGUILayout.EnumPopup("Outfit cell type", currentCell.outfitCellType);
        }


    }

}

