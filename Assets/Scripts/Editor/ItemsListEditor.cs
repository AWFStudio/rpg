﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ItemsList))]

public class ItemsListEditor : Editor
{
    private ItemsList itemsList;

    public override void OnInspectorGUI()//Внутри этой функции можно добавлять собственный графический интерфейс для инспектора
    {

        itemsList = (ItemsList)target;//ссылка на отображаемый в инспекторе скрипт
        SerializedObject serializedObject = new SerializedObject(itemsList);//сериализуем нужный скрипт
        serializedObject.Update();//и обновляем его

        SerializedProperty serializedPropertyList = serializedObject.FindProperty("Items");//указываем сериализуемые свойства (это будет все содержимое массива Quests)
        Draw(serializedPropertyList);//отрисовываем (выводим в инспекторе) весь список сериализумых свойств
    }

    private void Draw(SerializedProperty list)
    {
        EditorGUILayout.PropertyField(list);//создаем поле

        if (list.isExpanded)//если список помечен как отображаемый инспектором
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));//найти все его элементы
            ShowElements(list, false);//показать все элементы списка, но не показывать их названия ("элемент 1", "элемент 2" и т.д.)
        }

        if (GUILayout.Button("Add bag"))//если нажимаем
        {
            Bag bag = CreateInstance<Bag>();
            bag.name = "Bag";
            itemsList.Items.Add(bag);
            bag.ItemID = itemsList.Items.Count;
            AssetDatabase.AddObjectToAsset(bag, itemsList);
            AssetDatabase.SaveAssets();//сохраняем ассет

            if (!list.isExpanded)//если список помечен как не отображаемый
            {
                list.isExpanded = true;//то помечаем его, как отображаемый
            }
        }

        if (GUILayout.Button("Add weapon"))//если нажимаем
        {
            Weapon weapon = CreateInstance<Weapon>();
            weapon.name = "Weapon";
            itemsList.Items.Add(weapon);
            weapon.ItemID = itemsList.Items.Count;
            AssetDatabase.AddObjectToAsset(weapon, itemsList);
            AssetDatabase.SaveAssets();//сохраняем ассет

            if (!list.isExpanded)//если список помечен как не отображаемый
            {
                list.isExpanded = true;//то помечаем его, как отображаемый
            }
        }

        if (GUILayout.Button("Add consumable"))//если нажимаем
        {
            Consumable consumable = CreateInstance<Consumable>();
            consumable.name = "Consumable";
            itemsList.Items.Add(consumable);
            consumable.ItemID = itemsList.Items.Count;
            AssetDatabase.AddObjectToAsset(consumable, itemsList);
            AssetDatabase.SaveAssets();//сохраняем ассет

            if (!list.isExpanded)//если список помечен как не отображаемый
            {
                list.isExpanded = true;//то помечаем его, как отображаемый
            }
        }
    }

    private void ShowElements(SerializedProperty list, bool showElementLabels = true)
    {
        for (int i = 0; i < list.arraySize; i++)//для каждого элемента списка
        {
            EditorGUILayout.BeginHorizontal();//заключаем следующий код в этот контейнер, чтобы кнопка удаления элемента выводилась справа от него, а не под ним
            if (showElementLabels)//если опция "показывать названия элементов" включена
            {
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));//показываем названия элементов
            }
            else//иначе
            {
                EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), GUIContent.none);//скрываем названия элементов
            }
            //добавляем кнопку, чтобы удалять ненужные элементы
            if (GUILayout.Button("Delete", EditorStyles.miniButtonRight, GUILayout.ExpandWidth(false)))//создаем кнопку, указываем ей стиль, запрещаем растягивать ее по горизонтали
            {
                //если она нажата
                DestroyImmediate(list.GetArrayElementAtIndex(i).objectReferenceValue, true);//удаляем квест из ассетов
                itemsList.Items.RemoveAt(i);//и затем удаляем его из инспектора
                AssetDatabase.SaveAssets();//сохраняем ассет
            }
            EditorGUILayout.EndHorizontal();//закрываем контейнер с выравниванием

        }
    }
}

