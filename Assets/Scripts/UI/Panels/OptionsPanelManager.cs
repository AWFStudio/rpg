﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//висит на панели опций

public class OptionsPanelManager : MonoBehaviour
{
    TextAsset asset;
    XMLSettings UIelement;

    [SerializeField] Slider musicLevelSlider;
    [SerializeField] Slider soundLevelSlider;
    [SerializeField] Slider graphicsLevelSlider;

    [SerializeField] Text musicVolume;
    [SerializeField] Text soundVolume;
    [SerializeField] Text graphicsLevel;
    [SerializeField] Text selectLanguage;
    [SerializeField] Text exitOptions;
    [SerializeField] Text levelSelection;

    [SerializeField] Dropdown languageSelectionDropdown;

    void Start()
    {
        ApplyLanguageSettings();
    }

    //настройка громкости музыки и звука
    void Update()
    {
        Settings.musicLevel = (int)musicLevelSlider.value;
        Settings.soundLevel = (int)soundLevelSlider.value;
    }

    ////настройки графики
    //public void GraphicsSettings()
    //{
    //    QualitySettings.SetQualityLevel((int)graphicsLevelSlider.value, true);
    //}

    //настройки языка (только для панели настроек в главном меню)
    public void ChangeLanguage()
    {
        if (languageSelectionDropdown.value == 0)
        {
            LocalizationManager.currentLanguage = "ru_RU";
            ApplyLanguageSettings();
        }
        else
        {
            LocalizationManager.currentLanguage = "en_US";
            ApplyLanguageSettings();
        }
    }

    public void ApplyLanguageSettings()//локализация элементов UI
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Main Menu");
        UIelement = XMLSettings.Load(asset);

        musicVolume.text = UIelement.UIelements[5].text;
        soundVolume.text = UIelement.UIelements[6].text;
        graphicsLevel.text = UIelement.UIelements[7].text;
        exitOptions.text = UIelement.UIelements[9].text;
        levelSelection.text = UIelement.UIelements[11].text;

        if (selectLanguage != null)
        {
            selectLanguage.text = UIelement.UIelements[8].text;
        }
    }

}
