﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.IO;

//этот скрипт отвечает за выбор уровня сложности игры
//висит на соответствующей панели

public class LevelSelectionManager : MonoBehaviour
{
    [Header("Выбор уровня сложности")]
    [SerializeField]
    Text selectLevelTitleText;
    [SerializeField]
    Text adventureLvlTitle;
    [SerializeField]
    Text tacticsLvlTitle;
    [SerializeField]
    Text weightLimitText1;
    [SerializeField]
    Text weightLimitText2;
    [SerializeField]
    Text innerWarningText;
    [SerializeField]
    int selectedLevel;

    //прочее
    TextAsset asset;
    XMLSettings UIelement;

    public int SelectedLevel { get => selectedLevel; set => selectedLevel = value; }

    void Start()
    {
        SelectedLevel = 0;
        ApplyLanguageSettings();
    }

    //запоминаем уровень сложности
    public void SelectedAdventureLvl(bool isOn)
    {
        SelectedLevel = 1;
    }
    public void SelectedTacticsLvl(bool isOn)
    {
        SelectedLevel = 2;
    }

    void ApplyLanguageSettings()//локализация элементов UI
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        UIelement = XMLSettings.Load(asset);

        //выбор уровня сложности
        selectLevelTitleText.text = UIelement.UIelements[68].text;
        adventureLvlTitle.text = UIelement.UIelements[69].text;
        tacticsLvlTitle.text = UIelement.UIelements[70].text;
        weightLimitText1.text = UIelement.UIelements[71].text;
        weightLimitText2.text = UIelement.UIelements[72].text;
        innerWarningText.text = UIelement.UIelements[73].text;
    }
}
