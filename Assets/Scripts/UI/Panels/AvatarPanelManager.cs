﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//выводит аватар мембера
//висит на панели аватара

public class AvatarPanelManager : MonoBehaviour
{
    public GameObject characterAvatar;
    public GameObject character;

    void Start()
    {
        GameObject avatar = Instantiate(characterAvatar) as GameObject;
        avatar.transform.SetParent(this.transform, false);
        avatar.transform.SetAsFirstSibling();
        FocusManager focus = avatar.AddComponent<FocusManager>();
        focus.character = character;
    }
}
