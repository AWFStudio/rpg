﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//висит на панели характеристик

public class UIStatsPanelManager : MonoBehaviour
{
    TextAsset castomizationAsset;
    XMLSettings castomization;
    GroupManager group;
    GameObject character;
    int freePoints = 0;

    [Header("характеристики (наименование)")]
    [SerializeField]
    Text strTxt;
    [SerializeField]
    Text dexTxt;
    [SerializeField]
    Text constTxt;
    [SerializeField]
    Text intTxt;
    [SerializeField]
    Text percTxt;

    [Header("характеристики (значение)")]
    [SerializeField]
    Text strIntText;
    [SerializeField]
    Text dexIntText;
    [SerializeField]
    Text constIntText;
    [SerializeField]
    Text intIntText;
    [SerializeField]
    Text percIntText;
    [SerializeField]
    Text freeStatsIntText;

    [Header("модификаторы характеристик (значение)")]
    [SerializeField]
    Text strModText;
    [SerializeField]
    Text dexModText;
    [SerializeField]
    Text constModText;
    [SerializeField]
    Text intModText;
    [SerializeField]
    Text percModText;

    [Header("прочее")]
    [SerializeField]
    Text freeStatsTxt;
    [SerializeField]
    Text statsTitleText;

    public GameObject Character { get => character; set => character = value; }

    void Start()
    {
        group = FindObjectOfType<GroupManager>();
        Character = group.members[0];
        ApplyLanguageSettings();
        ApplyStats();
    }

    public void ApplyLanguageSettings()
    {
        castomizationAsset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        castomization = XMLSettings.Load(castomizationAsset);

        //характеристики
        strTxt.text = castomization.UIelements[0].text;
        dexTxt.text = castomization.UIelements[1].text;
        constTxt.text = castomization.UIelements[2].text;
        intTxt.text = castomization.UIelements[3].text;
        percTxt.text = castomization.UIelements[5].text;

        //свободных очков
        freeStatsTxt.text = castomization.UIelements[6].text;

        //Заголовок
        statsTitleText.text = castomization.UIelements[11].text;
    }

    public void ApplyStats()
    {
        //указываем значение характеристик
        strIntText.text = Character.GetComponent<CharacterProfile>().StrBase.ToString();
        dexIntText.text = Character.GetComponent<CharacterProfile>().DexBase.ToString();
        constIntText.text = Character.GetComponent<CharacterProfile>().ConstBase.ToString();
        intIntText.text = Character.GetComponent<CharacterProfile>().IntBase.ToString();
        percIntText.text = Character.GetComponent<CharacterProfile>().PercBase.ToString();

        //указываем значение модификаторов характеристик
        strModText.text = Character.GetComponent<CharacterProfile>().StrMod.ToString();
        dexModText.text = Character.GetComponent<CharacterProfile>().DexMod.ToString();
        constModText.text = Character.GetComponent<CharacterProfile>().ConstMod.ToString();
        intModText.text = Character.GetComponent<CharacterProfile>().IntMod.ToString();
        percModText.text = Character.GetComponent<CharacterProfile>().PercMod.ToString();

        freeStatsIntText.text = freePoints.ToString();

    }

}
