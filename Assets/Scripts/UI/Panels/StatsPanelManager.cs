﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//висит на панели характеристик в окне кастомизации

public class StatsPanelManager : MonoBehaviour
{
    [Header("заголовок")]
    [SerializeField]
    Text statsTitleText;

    [Header("свободные очки")]
    [SerializeField]
    Text freeStatsTxt;
    [SerializeField]
    Text freeStatsIntText;

    [Header("характеристики (наименование)")]
    [SerializeField]
    Text strTxt;
    [SerializeField]
    Text dexTxt;
    [SerializeField]
    Text constTxt;
    [SerializeField]
    Text intTxt;
    [SerializeField]
    Text percTxt;

    [Header("характеристики (значения)")]
    [SerializeField]
    Text strIntText;
    [SerializeField]
    Text dexIntText;
    [SerializeField]
    Text constIntText;
    [SerializeField]
    Text intIntText;
    [SerializeField]
    Text percIntText;

    [Header("модификаторы характеристик (значения)")]
    [SerializeField]
    Text strModText;
    [SerializeField]
    Text dexModText;
    [SerializeField]
    Text constModText;
    [SerializeField]
    Text intModText;
    [SerializeField]
    Text percModText;

    //характеристики (значения)
    int strBase;
    int dexBase;
    int constBase;
    int intBase;
    int percBase;

    //модификаторы характеристик(значения)
    int strMod;
    int dexMod;
    int intMod;
    int percMod;
    int constMod;

    //свободных очков характеристик
    int freeStats;

    //прочее
    TextAsset asset;
    XMLSettings UIelement;
    CharacterSelectionManager character;
    StatisticsPanelManager statistics;

    public int StrBase { get => strBase; set => strBase = value; }

    void Start()
    {
        character = FindObjectOfType<CharacterSelectionManager>();
        ApplyLanguageSettings();
        ResetStats();
    }

    public void ApplyLanguageSettings()
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        UIelement = XMLSettings.Load(asset);

        //характеристики
        strTxt.text = UIelement.UIelements[6].text;
        dexTxt.text = UIelement.UIelements[7].text;
        constTxt.text = UIelement.UIelements[8].text;
        intTxt.text = UIelement.UIelements[9].text;
        percTxt.text = UIelement.UIelements[10].text;

        //свободных очков
        freeStatsTxt.text = UIelement.UIelements[11].text;

        //Заголовок
        statsTitleText.text = UIelement.UIelements[12].text;
    }

    public void ResetStats()
    {
        //характеристики (значения)
        StrBase = 5;
        dexBase = 5;
        constBase = 5;
        intBase = 5;
        percBase = 5;

        //модификаторы характеристик(значения)
        strMod = 0;
        dexMod = 0;
        intMod = 0;
        percMod = 0;
        constMod = 0;

        //свободных очков характеристик
        freeStats = 0;

        UpdateStatsInfo();
    }

    public void UpdateStatsInfo()
    {
        //указываем значения характеристик
        strIntText.text = StrBase.ToString();
        dexIntText.text = dexBase.ToString();
        constIntText.text = constBase.ToString();
        intIntText.text = intBase.ToString();
        percIntText.text = percBase.ToString();
        freeStatsIntText.text = freeStats.ToString();

        //рассчитываем модификаторы характеристик
        strMod = StrBase / 3;
        dexMod = dexBase / 3;
        intMod = intBase / 3;
        percMod = percBase / 3;
        constMod = constBase / 3;

        //выводим значения модификаторов характеристик
        strModText.text = "+ " + strMod.ToString();
        dexModText.text = "+ " + dexMod.ToString();
        intModText.text = "+ " + intMod.ToString();
        percModText.text = "+ " + percMod.ToString();
        constModText.text = "+ " + constMod.ToString();

        statistics.UpdateStatistics();
    }

    //для кнопок "+"
    public void AddPoint(string stat)
    {
        if (stat == "Str")
        {
            if (freeStats > 0)
            {
                StrBase++;
                freeStats--;
            }
        }
        else if (stat == "Dex")
        {
            if (freeStats > 0)
            {
                dexBase++;
                freeStats--;
            }
        }
        else if (stat == "Const")
        {
            if (freeStats > 0)
            {
                constBase++;
                freeStats--;
            }
        }
        else if (stat == "Int")
        {
            if (freeStats > 0)
            {
                intBase++;
                freeStats--;
            }
        }
        else if (stat == "Perc")
        {
            if (freeStats > 0)
            {
                percBase++;
                freeStats--;
            }
        }
        UpdateStatsInfo();
    }

    //для кнопок "-"
    public void TakeOffPoint(string stat)
    {
        if (stat == "Str")
        {
            if (StrBase > 1)
            {
                StrBase--;
                freeStats++;
            }
        }
        else if (stat == "Dex")
        {
            if (dexBase > 1)
            {
                dexBase--;
                freeStats++;
            }

        }
        else if (stat == "Const")
        {
            if (constBase > 1)
            {
                constBase--;
                freeStats++;
            }

        }
        else if (stat == "Int")
        {
            if (intBase > 1)
            {
                intBase--;
                freeStats++;
            }
        }
        else if (stat == "Perc")
        {
            if (percBase > 1)
            {
                percBase--;
                freeStats++;
            }
        }
        UpdateStatsInfo();
    }





}
