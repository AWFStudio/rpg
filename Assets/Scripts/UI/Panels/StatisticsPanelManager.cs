﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.IO;

//висит на панели статистики в окне кастомизации

public class StatisticsPanelManager : MonoBehaviour
{
    [Header("лимит переносимого веса")]
    [SerializeField]
    Text weightLimitText;
    [SerializeField]
    Text weightLimitCountText;

    //прочее
    TextAsset asset;
    XMLSettings UIelement;
    CharacterSelectionManager character;
    StatsPanelManager stats;

    void Start()
    {
        character = FindObjectOfType<CharacterSelectionManager>();
        stats = FindObjectOfType<StatsPanelManager>();
        ApplyLanguageSettings();
    }

    void ApplyLanguageSettings()//локализация элементов UI
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        UIelement = XMLSettings.Load(asset);

        weightLimitText.text = UIelement.UIelements[29].text;
    }

   public void UpdateStatistics()
    {
        weightLimitCountText.text = (stats.StrBase*10).ToString() + UIelement.UIelements[30].text;
    }


}
