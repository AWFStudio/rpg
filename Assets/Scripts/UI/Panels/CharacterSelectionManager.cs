﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.IO;

//скрипт панели выбора персонажа в окне кастомизации

public class CharacterSelectionManager : MonoBehaviour
{
    [Header("Выбор персонажа")]
    [SerializeField]
    GameObject ValkarImg;
    [SerializeField]
    GameObject KoredImg;
    [SerializeField]
    GameObject GrundbuksaImg;
    [SerializeField]
    GameObject RiiseImg;
    [SerializeField]
    Text nameText;
    [SerializeField]
    string selectedCharacter;

    [Header("раса")]
    [SerializeField]
    Text raceText;
    [SerializeField]
    Text raceOfThisCharacterText;

    [Header("класс")]
    [SerializeField]
    Text classText;
    [SerializeField]
    Text classOfThisCharacterText;

    [Header("панели")]
    [SerializeField]
    GameObject featuresPanel;
    [SerializeField]
    GameObject statisticsPanel;

    //прочее
    TextAsset asset;
    XMLSettings UIelement;

    FeaturesPanelManager features;
    StatsPanelManager stats;

    public string SelectedCharacter { get => selectedCharacter; set => selectedCharacter = value; }

    public void Awake()
    {
        SelectedCharacter = "Valkar";
        features = FindObjectOfType<FeaturesPanelManager>();
    }

    void Start()
    {
        ApplyLanguageSettings();
    }

    //листаем персонажей вперед
    public void ArrowForwardBttn()
    {
        if (ValkarImg.activeSelf)
        {
            ValkarImg.SetActive(false);
            KoredImg.SetActive(true);
            SelectedCharacter = "Kored";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
        else if (KoredImg.activeSelf)
        {
            KoredImg.SetActive(false);
            GrundbuksaImg.SetActive(true);
            SelectedCharacter = "Grundbuksa";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
        else if (GrundbuksaImg.activeSelf)
        {
            GrundbuksaImg.SetActive(false);
            RiiseImg.SetActive(true);
            SelectedCharacter = "Riise";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
        else if (RiiseImg.activeSelf)
        {
            RiiseImg.SetActive(false);
            ValkarImg.SetActive(true);
            SelectedCharacter = "Valkar";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
    }

    //листаем персонажей назад
    public void ArrowBackBttn()
    {
        if (ValkarImg.activeSelf)
        {
            ValkarImg.SetActive(false);
            RiiseImg.SetActive(true);
            SelectedCharacter = "Riise";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
        else if (RiiseImg.activeSelf)
        {
            RiiseImg.SetActive(false);
            GrundbuksaImg.SetActive(true);
            SelectedCharacter = "Grundbuksa";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
        else if (GrundbuksaImg.activeSelf)
        {
            GrundbuksaImg.SetActive(false);
            KoredImg.SetActive(true);
            SelectedCharacter = "Kored";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
        else if (KoredImg.activeSelf)
        {
            KoredImg.SetActive(false);
            ValkarImg.SetActive(true);
            SelectedCharacter = "Valkar";
            ResetCharacterOptions();
            ApplyCharactersInfo();
        }
    }
    void ApplyLanguageSettings()//локализация элементов UI
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        UIelement = XMLSettings.Load(asset);

        raceText.text = UIelement.UIelements[17].text;
        classText.text = UIelement.UIelements[21].text;

        //личная информация персонажей
        ApplyCharactersInfo();
    }

    //отображаем инфу о текущем выбранном персонаже
    public void ApplyCharactersInfo()
    {
        if (SelectedCharacter == "Valkar")
        {
            nameText.text = UIelement.UIelements[2].text;
            raceOfThisCharacterText.text = UIelement.UIelements[18].text;
            classOfThisCharacterText.text = UIelement.UIelements[22].text;
        }
        else if (SelectedCharacter == "Kored")
        {
            nameText.text = UIelement.UIelements[3].text;
            raceOfThisCharacterText.text = UIelement.UIelements[18].text;
            classOfThisCharacterText.text = UIelement.UIelements[23].text;
        }
        else if (SelectedCharacter == "Grundbuksa")
        {
            nameText.text = UIelement.UIelements[4].text;
            raceOfThisCharacterText.text = UIelement.UIelements[19].text;
            classOfThisCharacterText.text = UIelement.UIelements[24].text;
        }
        else if (SelectedCharacter == "Riise")
        {
            nameText.text = UIelement.UIelements[5].text;
            raceOfThisCharacterText.text = UIelement.UIelements[20].text;
            classOfThisCharacterText.text = UIelement.UIelements[25].text;
        }
        features.ApplyCharactersInfo();
    }

    void ResetCharacterOptions()
    {
        if (statisticsPanel.activeSelf)
        {
            if(stats == null)
            {
                stats = FindObjectOfType<StatsPanelManager>();
            }
            stats.ResetStats();
            statisticsPanel.SetActive(false);
            featuresPanel.SetActive(true);
        }
    }

}
