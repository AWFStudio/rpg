﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//скрипт инвентаря
//висит на панели инвентаря

public class InventoryPanelManager : MonoBehaviour
{
    public GameObject contentPanel;//панель ячеек инвентаря
    public Dropdown sortingInventory;//сортировщик инвентаря
    public ItemCell[] inventoryCells;//массив ячеек инвентаря
    public Text groupWeightText;//лимит веса группы и текущий вес инвентаря

    int inventoryCapacity;//вместимость инвентаря
    int maxGroupWeight;//лимит переносимого веса группы
    float currentInventoryWeight;//вес предметов в инвентаре

    GroupManager group;//группа
    TextAsset UIAsset;
    XMLSettings UI;

    //опции сортировщика:
    string cost;
    string weight;

    public int InventoryCapacity { get => inventoryCapacity; set => inventoryCapacity = value; }

    public void Awake()
    {
        group = FindObjectOfType<GroupManager>();
    }

    public void Start()
    {
        AddSortingOptions();
    }

    //создаем необходимое количество ячеек инвентаря
    public void CreateCells()
    {
        for (int i = 0; i < InventoryCapacity; i++)//перебираем инвентарь группы
        {
            if (inventoryCells[i] == null)
            {
                GameObject inventoryCell = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/UI Elements/Inventory Cell"));//инстанциируем ячейку
                inventoryCell.transform.SetParent(contentPanel.transform, false);//устанавливаем родительский элемент
                inventoryCell.GetComponent<ItemCell>().cellType = ItemCell.CellType.inventory;//помечаем, что это ячейка инвентаря
                inventoryCells[i] = inventoryCell.GetComponent<ItemCell>();//записываем в массив
                inventoryCell.name = "Cell " + (i + 1);//меняем имя в инспекторе
            }
        }
    }

    //добавляем предмет в инвентарь к уже существующему предмету
    public bool FindAndAddItem(Item item)
    {
        UpdateCellsAmount();
        for (int i = 0; i < InventoryCapacity; i++)
        {
            if ((inventoryCells[i] != null)
                && (inventoryCells[i].transform.childCount > 0)
                && (inventoryCells[i].GetComponentInChildren<ItemComponent>().item.ItemID == item.ItemID)
                && (inventoryCells[i].GetComponentInChildren<ItemComponent>().item.InCell < item.InCell))
            {
                inventoryCells[i].GetComponentInChildren<Item>().InCell++;
                return true;
            }
        }
        return false;
    }
    //добавляем новый предмет в инвентарь
    public bool AddNewItem(Item item)
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if (inventoryCells[i].transform.childCount == 0)
            {
                GameObject newItem = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Inventory Items/Item"));
                newItem.GetComponent<ItemComponent>().item = item;
                newItem.GetComponent<Image>().sprite = newItem.GetComponent<ItemComponent>().item.ItemIcon;
                newItem.transform.SetParent(inventoryCells[i].transform, false);
                return true;
            }
        }
        return false;
    }

    //обновляем количество ячеек инвентаря
    public void UpdateCellsAmount()
    {
        InventoryCapacity = 0;

        for (int i = 0; i < group.groupMembersCount; i++)//пробегаемся по всем мемберам в группе
        {
            if (group.members[i].GetComponent<CharacterEquipment>().bag != null)//если у мембера есть сумка
            {
                int bagCells = group.members[i].GetComponent<CharacterEquipment>().bag.BagCellsAmount;//считаем ячейки в ней
                InventoryCapacity += bagCells;//и добавляем их к общему количеству ячеек инвентаря
            }
        }
        if (inventoryCells.Length == 0)//если не создано ни одной ячейки 
        {
            inventoryCells = new ItemCell[InventoryCapacity];//считаем, сколько ячеек должно быть в инвентаре
            CreateCells();//создаем ячейки
        }
        if (inventoryCells.Length != 0 && inventoryCells.Length < InventoryCapacity)//если ячейки есть, но их меньше, чем должно быть
        {
            System.Array.Resize(ref inventoryCells, InventoryCapacity);//меняем размер массива ячеек
            CreateCells();//создаем недостающие ячейки
        }
    }
    //показать все предметы в инвентаре
    public void ShowAll()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if (inventoryCells[i] != null)
            {
                inventoryCells[i].gameObject.SetActive(true);
            }
        }
    }
    //показать только оружие
    public void ShowOnlyWeapon()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if ((inventoryCells[i] != null)
                && (inventoryCells[i].transform.childCount != 0)
                && (inventoryCells[i].GetComponentInChildren<Item>() != null))
            {
                inventoryCells[i].gameObject.SetActive(false);
            }
            else
            {
                inventoryCells[i].gameObject.SetActive(true);
            }
        }
    }
    //показать щиты и броню
    public void ShowOnlyArmorAndShields()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if ((inventoryCells[i] != null)
                && (inventoryCells[i].transform.childCount != 0)
                && (inventoryCells[i].GetComponentInChildren<Item>().ItemAppointment != Item.Item_Appointment.armor || inventoryCells[i].GetComponentInChildren<Item>().ItemAppointment != Item.Item_Appointment.shield))
            {
                inventoryCells[i].gameObject.SetActive(false);
            }
            else
            {
                inventoryCells[i].gameObject.SetActive(true);
            }
        }
    }
    //показать только аксессуары
    public void ShowOnlyAccessories()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if ((inventoryCells[i] != null)
                && (inventoryCells[i].transform.childCount != 0)
                && (inventoryCells[i].GetComponentInChildren<Item>().ItemAppointment != Item.Item_Appointment.accessory))
            {
                inventoryCells[i].gameObject.SetActive(false);
            }
            else
            {
                inventoryCells[i].gameObject.SetActive(true);
            }
        }
    }
    //показать только расходники
    public void ShowOnlyConsumables()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if ((inventoryCells[i] != null)
                && (inventoryCells[i].transform.childCount != 0)
                && (inventoryCells[i].GetComponentInChildren<Item>().ItemAppointment != Item.Item_Appointment.consumable))
            {
                inventoryCells[i].gameObject.SetActive(false);
            }
            else
            {
                inventoryCells[i].gameObject.SetActive(true);
            }
        }
    }
    //показать только ингредиенты
    public void ShowOnlyIngredients()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if ((inventoryCells[i] != null)
                && (inventoryCells[i].transform.childCount != 0)
                && (inventoryCells[i].GetComponentInChildren<Item>().ItemAppointment != Item.Item_Appointment.ingredient))
            {
                inventoryCells[i].gameObject.SetActive(false);
            }
            else
            {
                inventoryCells[i].gameObject.SetActive(true);
            }
        }
    }
    //показать только особые/квестовые
    public void ShowOnlySpecial()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            if ((inventoryCells[i] != null)
                && (inventoryCells[i].transform.childCount != 0)
                && (inventoryCells[i].GetComponentInChildren<Item>().ItemAppointment != Item.Item_Appointment.special))
            {
                inventoryCells[i].gameObject.SetActive(false);
            }
            else
            {
                inventoryCells[i].gameObject.SetActive(true);
            }
        }
    }

    //сортировать предметы в инвентаре
    public void Sort()
    {
        if (sortingInventory.value == 0)//по умолчанию
        {
            AutoSort();
        }
        else if (sortingInventory.value == 1)//по стоимости
        {
            AutoSort();
            SortByCost();
        }
        else if (sortingInventory.value == 2)//по весу
        {
            AutoSort();
            SortByWeight();
        }
    }

    //сортировка по стоимости
    void SortByCost()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            for (int j = 0; j < inventoryCells.Length - 1; j++)
            {
                if (inventoryCells[j].transform.childCount > 0
                    && inventoryCells[j + 1].transform.childCount > 0
                    && inventoryCells[j].GetComponentInChildren<Item>().ItemCost < inventoryCells[j + 1].GetComponentInChildren<Item>().ItemCost)
                {
                    GameObject temp1 = inventoryCells[j + 1].GetComponentInChildren<IconManager>().gameObject;
                    GameObject temp2 = inventoryCells[j].GetComponentInChildren<IconManager>().gameObject;
                    temp1.transform.position = inventoryCells[j].transform.position;
                    temp2.transform.position = inventoryCells[j + 1].transform.position;
                    temp1.transform.SetParent(inventoryCells[j].transform);
                    temp2.transform.SetParent(inventoryCells[j + 1].transform);
                    temp1.GetComponent<IconManager>().Cell = inventoryCells[j];
                    temp2.GetComponent<IconManager>().Cell = inventoryCells[j + 1];
                }
            }
        }
    }

    //сортировка по весу
    void SortByWeight()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            for (int j = 0; j < inventoryCells.Length - 1; j++)
            {
                if (inventoryCells[j].transform.childCount > 0
                    && inventoryCells[j + 1].transform.childCount > 0
                    && inventoryCells[j].GetComponentInChildren<Item>().ItemWeight < inventoryCells[j + 1].GetComponentInChildren<Item>().ItemWeight)
                {
                    GameObject temp1 = inventoryCells[j + 1].GetComponentInChildren<IconManager>().gameObject;
                    GameObject temp2 = inventoryCells[j].GetComponentInChildren<IconManager>().gameObject;
                    temp1.transform.position = inventoryCells[j].transform.position;
                    temp2.transform.position = inventoryCells[j + 1].transform.position;
                    temp1.transform.SetParent(inventoryCells[j].transform);
                    temp2.transform.SetParent(inventoryCells[j + 1].transform);
                    temp1.GetComponent<IconManager>().Cell = inventoryCells[j];
                    temp2.GetComponent<IconManager>().Cell = inventoryCells[j + 1];
                }
            }
        }
    }

    //автосортировка
    void AutoSort()
    {
        for (int i = 0; i < inventoryCells.Length; i++)
        {
            for (int j = 0; j < inventoryCells.Length - 1; j++)
            {
                if ((inventoryCells[j].transform.childCount == 0)
                    && (inventoryCells[j + 1].transform.childCount > 0))
                {
                    GameObject temp = inventoryCells[j + 1].GetComponentInChildren<IconManager>().gameObject;
                    temp.transform.position = inventoryCells[j].transform.position;
                    temp.transform.SetParent(inventoryCells[j].transform);
                    temp.GetComponent<IconManager>().Cell = inventoryCells[j];
                }
            }
        }
    }

    public void AddSortingOptions()//добавляем опции в сортировщик
    {
        ApplyLanguageSettings();
        sortingInventory.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = "..." });
        sortingInventory.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = cost });
        sortingInventory.GetComponent<Dropdown>().options.Add(new Dropdown.OptionData() { text = weight });
    }

    public void ApplyLanguageSettings()
    {
        UIAsset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Main UI");
        UI = XMLSettings.Load(UIAsset);

        //выпадающий список сортировки инвентаря
        cost = UI.UIelements[15].text;
        weight = UI.UIelements[16].text;
    }

    //обновляем инфо о весе
    public void UpdateGroupWeight()
    {
        maxGroupWeight = 0;
        currentInventoryWeight = 0;
        for (int i = 0; i < group.groupMembersCount; i++)
        {
            maxGroupWeight += group.members[i].GetComponent<CharacterProfile>().WeightLimit;
        }
        for (int i = 0; i < InventoryCapacity; i++)
        {
            if (inventoryCells[i].transform.childCount > 0)
            {
                float itemWeight = inventoryCells[i].GetComponentInChildren<ItemComponent>().item.ItemWeight;
                int itemCount = inventoryCells[i].GetComponentInChildren<ItemComponent>().item.InCell;
                currentInventoryWeight += (itemWeight * itemCount);
            }
        }
        groupWeightText.text = currentInventoryWeight.ToString() + "/" + maxGroupWeight.ToString();
    }

}


