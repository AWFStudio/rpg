﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//висит на панели с "куклами" персонажей

public class OutfitPanelManager : MonoBehaviour
{
    public GameObject character;

    UIStatsPanelManager stats;
    UIStatisticsPanelManager statistics;
    GroupManager group;

    [Header("Ячейки слева")]
    public GameObject headCell;
    public GameObject pauldronCell;
    public GameObject chestCell;
    public GameObject legsCell;
    public GameObject handsCell;
    public GameObject feetCell;
    [Header("Ячейки справа")]
    public GameObject neckCell;
    public GameObject ring1Cell;
    public GameObject ring2Cell;
    public GameObject bagCell;
    public GameObject beltCell;
    public GameObject acessoryCell;
    [Header("Оружейные ячейки")]
    public GameObject rightWeaponCell;
    public GameObject leftWeaponCell;

    void Start()
    {
        stats = FindObjectOfType<UIStatsPanelManager>();
        statistics = FindObjectOfType<UIStatisticsPanelManager>();
        group = FindObjectOfType<GroupManager>();
        character = group.members[0];
        UpdateOutfit();
    }

    //при выборе другого мембера, обновляем "куклу"
    public void UpdateOutfit()
    {
        //сначала "затираем" все содержимое ячеек "куклы"
        if (bagCell.transform.childCount != 0)
        {
            Destroy(bagCell.GetComponentInChildren<ItemComponent>().gameObject);
        }
        if (rightWeaponCell.transform.childCount != 0)
        {
            Destroy(rightWeaponCell.GetComponentInChildren<ItemComponent>().gameObject);
        }
        if (leftWeaponCell.transform.childCount != 0)
        {
            Destroy(leftWeaponCell.GetComponentInChildren<ItemComponent>().gameObject);
        }

        //потом выводим новое:
        //сумка:
        if (character.GetComponent<CharacterEquipment>().bag != null)
        {
            GameObject bag = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Inventory Items/Item"));
            bag.GetComponent<ItemComponent>().item = character.GetComponent<CharacterEquipment>().bag;
            bag.GetComponent<Image>().sprite = bag.GetComponent<ItemComponent>().item.ItemIcon;
            bag.transform.SetParent(bagCell.transform, false);
        }
        //оружие в правой руке
        if (character.GetComponent<CharacterEquipment>().rightWeapon != null)
        {
            GameObject rightWeapon = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Inventory Items/Item"));
            rightWeapon.GetComponent<ItemComponent>().item = character.GetComponent<CharacterEquipment>().rightWeapon;
            rightWeapon.GetComponent<Image>().sprite = rightWeapon.GetComponent<ItemComponent>().item.ItemIcon;
            rightWeapon.transform.SetParent(rightWeaponCell.transform, false);
        }
        //оружие в левой руке
        if (character.GetComponent<CharacterEquipment>().leftWeapon != null)
        {
            GameObject leftWeapon = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Inventory Items/Item"));
            leftWeapon.GetComponent<ItemComponent>().item = character.GetComponent<CharacterEquipment>().leftWeapon;
            leftWeapon.GetComponent<Image>().sprite = leftWeapon.GetComponent<ItemComponent>().item.ItemIcon;
            leftWeapon.transform.SetParent(leftWeaponCell.transform, false);
        }

    }

    //когда выбираем другого мембера, обновляем связанные с ним панели
    public void ChangeSelectedMember(int member)
    {
        if (member == 0)
        {
            character = group.members[0];
            UpdateOutfit();
            stats.Character = group.members[0];
            stats.ApplyStats();
            statistics.character = group.members[0];
            statistics.ApplyLanguageSettings();
            statistics.UpdateStatistics();
        }
        else if (member == 1)
        {
            character = group.members[1];
            UpdateOutfit();
            stats.Character = group.members[1];
            stats.ApplyStats();
            statistics.character = group.members[1];
            statistics.ApplyLanguageSettings();
            statistics.UpdateStatistics();
        }
        else if (member == 2)
        {
            character = group.members[2];
            UpdateOutfit();
            stats.Character = group.members[2];
            stats.ApplyStats();
            statistics.character = group.members[2];
            statistics.ApplyLanguageSettings();
            statistics.UpdateStatistics();
        }
        else if (member == 3)
        {
            character = group.members[3];
            UpdateOutfit();
            stats.Character = group.members[3];
            stats.ApplyStats();
            statistics.character = group.members[3];
            statistics.ApplyLanguageSettings();
            statistics.UpdateStatistics();
        }
    }
}
