﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.IO;

//висит на панели индивидуальных черт

public class FeaturesPanelManager : MonoBehaviour
{

    [Header("индивидуальные особенности")]
    [SerializeField]
    GameObject featuresPanel;
    [SerializeField]
    Text featuresTitleText;
    [SerializeField]
    GameObject[] features;

    //прочее
    TextAsset asset;
    XMLSettings UIelement;
    CharacterSelectionManager character;

    void Start()
    {
        character = FindObjectOfType<CharacterSelectionManager>();
        featuresPanel = gameObject;
        features = new GameObject[4];
        for (int i = 0; i < features.Length; i++)
        {
            features[i] = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/UI Elements/Feature Panel"));
            features[i].transform.SetParent(featuresPanel.transform);
            features[i].transform.position = featuresPanel.transform.position;
        }
        features[3].transform.GetChild(2).gameObject.SetActive(false);

        ApplyLanguageSettings();
    }

    void ApplyLanguageSettings()//локализация элементов UI
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        UIelement = XMLSettings.Load(asset);

        featuresTitleText.text = UIelement.UIelements[14].text;

        //личная информация персонажей
        ApplyCharactersInfo();
    }

    public void ApplyCharactersInfo()
    {
        if (character.SelectedCharacter == "Valkar")
        {
            features[0].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[36].text;
            features[0].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[37].text;

            features[1].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[38].text;
            features[1].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[39].text;

            features[2].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[40].text;
            features[2].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[41].text;

            features[3].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[42].text;
            features[3].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[43].text;
        }
        else if (character.SelectedCharacter == "Kored")
        {
            features[0].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[44].text;
            features[0].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[45].text;

            features[1].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[46].text;
            features[1].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[47].text;

            features[2].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[48].text;
            features[2].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[49].text;

            features[3].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[50].text;
            features[3].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[51].text;
        }
        else if (character.SelectedCharacter == "Grundbuksa")
        {
            features[0].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[52].text;
            features[0].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[53].text;

            features[1].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[54].text;
            features[1].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[55].text;

            features[2].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[56].text;
            features[2].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[57].text;

            features[3].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[58].text;
            features[3].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[59].text;
        }
        else if (character.SelectedCharacter == "Riise")
        {
            features[0].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[60].text;
            features[0].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[61].text;

            features[1].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[62].text;
            features[1].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[63].text;

            features[2].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[64].text;
            features[2].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[65].text;

            features[3].GetComponent<FeatureManager>().featureTitleTxt.text = UIelement.UIelements[66].text;
            features[3].GetComponent<FeatureManager>().FeatureDescriptionTxt.text = UIelement.UIelements[67].text;
        }
    }

}
