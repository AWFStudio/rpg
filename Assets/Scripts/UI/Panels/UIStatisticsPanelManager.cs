﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//висит на панели статистики персонажа

public class UIStatisticsPanelManager : MonoBehaviour
{
    TextAsset castomizationAsset;
    XMLSettings UIelement;

    GroupManager group;

    public GameObject character;

    public Text characterInfoText;

    [Header("заголовок")]
    [SerializeField]
    Text statsTitleText;

    [Header("физ. урон")]
    [SerializeField]
    Text physicalDamageText;
    [SerializeField]
    Text physicalDamageCountText;

    [Header("лимит переносимого веса")]
    [SerializeField]
    Text weightLimitText;
    [SerializeField]
    Text weightLimitCountText;


    public void Start()
    {
        group = FindObjectOfType<GroupManager>();
        character = group.members[0];
        ApplyLanguageSettings();
        UpdateStatistics();
    }

    //статистика (выводим значения)
    public void UpdateStatistics()
    {
        UpdateDamageStatistics();
        UpdateWeightLimitStatistics();
    }

    public void ApplyLanguageSettings()
    {

        castomizationAsset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        UIelement = XMLSettings.Load(castomizationAsset);

        //статистика
        physicalDamageText.text = UIelement.UIelements[70].text;
        weightLimitText.text = UIelement.UIelements[31].text;

        //Заголовок
        statsTitleText.text = UIelement.UIelements[12].text;
    }

    //высчитываем урон
    public void UpdateDamageStatistics()
    {
        int min = 0;
        int max = 0;

        //если руки пусты
        if (character.GetComponent<CharacterEquipment>().rightWeapon == null && character.GetComponent<CharacterEquipment>().leftWeapon == null)
        {
            min += 1 + character.GetComponent<CharacterProfile>().StrMod;
            max += 2 + character.GetComponent<CharacterProfile>().StrMod;
        }
        //если в правой руке есть оружие
        if (character.GetComponent<CharacterEquipment>().rightWeapon != null)
        {
            min += character.GetComponent<CharacterEquipment>().rightWeapon.MinDamage;
            max += character.GetComponent<CharacterEquipment>().rightWeapon.MaxDamage;
            //если это арбалет
            if (character.GetComponent<CharacterEquipment>().rightWeapon.WeaponClass == Weapon.Weapon_Class.crossbow)
            {
                min += System.Convert.ToInt32(character.GetComponent<CharacterProfile>().DexMod * 1.5);
                max += System.Convert.ToInt32(character.GetComponent<CharacterProfile>().DexMod * 1.5);
            }
            //если это двуручный меч
            else if (character.GetComponent<CharacterEquipment>().rightWeapon.WeaponClass == Weapon.Weapon_Class.longSword)
            {
                min += System.Convert.ToInt32(character.GetComponent<CharacterProfile>().StrMod * 1.5);
                max += System.Convert.ToInt32(character.GetComponent<CharacterProfile>().StrMod * 1.5);
            }
            //иное оружие
            else
            {
                min += character.GetComponent<CharacterProfile>().StrMod;
                max += character.GetComponent<CharacterProfile>().StrMod;
            }
        }
        //если в левой руке есть оружие
        if (character.GetComponent<CharacterEquipment>().leftWeapon != null)
        {
            //и оно предназначено для левой руки
            if (character.GetComponent<CharacterEquipment>().leftWeapon.WeaponType == Weapon.Weapon_Type.leftHanded)
            {
                min += character.GetComponent<CharacterEquipment>().leftWeapon.MinDamage;
                max += character.GetComponent<CharacterEquipment>().leftWeapon.MaxDamage;
            }
            //и оно предназначено для любой руки
            else if (character.GetComponent<CharacterEquipment>().leftWeapon.WeaponType == Weapon.Weapon_Type.bothHanded)
            {
                min += character.GetComponent<CharacterEquipment>().leftWeapon.MinDamage / 2;
                max += character.GetComponent<CharacterEquipment>().leftWeapon.MaxDamage / 2;
            }
            min += character.GetComponent<CharacterProfile>().StrMod / 2;
            max += character.GetComponent<CharacterProfile>().StrMod / 2;
        }
        physicalDamageCountText.text = min.ToString() + " - " + max.ToString();
    }

    //высчитываем переносимый вес
    public void UpdateWeightLimitStatistics()
    {
        int weight;
        //для Кореда
        if (character.GetComponent<CharacterProfile>().CharacterName == "Kored")
        {
            weight = character.GetComponent<CharacterProfile>().StrBase * 5;

        }
        //для остальных персонажей
        else
        {
            weight = character.GetComponent<CharacterProfile>().StrBase * 10;
        }

        weightLimitCountText.text = weight.ToString() + " " + UIelement.UIelements[32].text;
    }



}
