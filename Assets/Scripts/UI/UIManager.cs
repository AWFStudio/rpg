﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ThePlace;

//это синглтон основного интерфейса

public class UIManager : Singleton<UIManager>
{
    [Header("панели")]

    public GameObject dialoguePanel;//диалог

    public GameObject pausePanel;//пауза
    public GameObject optionsPanel;//настройки

    public GameObject generalPanel;//главная
    public GameObject equipmentPanel;//снаряжение (инвентарь, кукла, выбранный мембер)

    //панель группы
    public static GameObject groupPanel;//группа (аватары)
    public static GameObject[] characterPanels;//мемберы (массив)
    public static GameObject characterPanel;//мембер

    [Header("панель снаряжения")]
    public GameObject innerAvatarsPanel;//панель с аватарами персонажей внутри главной панели
    public GameObject innerAvatarPanel0;
    public GameObject innerAvatarPanel1;
    public GameObject innerAvatarPanel2;
    public GameObject innerAvatarPanel3;

    //активность окон
    public static bool pausePanelIsActive = false;
    public static bool generaPanelIsActive = false;
    public static bool dialogPanelIsActive = false;
    public static bool messagePanelIsActive = false;
    public static bool equipmentPanelIsActive = false;
    public static bool optionsPanelIsActive = false;

    //прочее
    CameraMovement cameraMovement;
    public InventoryPanelManager inventory;
    GroupManager group;

    //аватары на панели снаряжения
    public static GameObject innerAvatar;
    public static GameObject[] innerAvatars;

    void Awake()
    {
        group = FindObjectOfType<GroupManager>();
        characterPanels = new GameObject[4];
        innerAvatars = new GameObject[4];
        groupPanel = GameObject.FindGameObjectWithTag("Group Panel");
        inventory = FindObjectOfType<InventoryPanelManager>();
    }

    void Start()
    {
        Time.timeScale = 1.0f;//запускаем время
        cameraMovement = FindObjectOfType<CameraMovement>();
    }

    void Update()
    {
        //если открыта главная панель, диалог или сообщение, запрещаем движение камеры
        if (generaPanelIsActive || dialogPanelIsActive || messagePanelIsActive)
        {
            cameraMovement.camMove = false;
            cameraMovement.camScroll = false;
        }
        else
        {
            cameraMovement.camMove = true;
            cameraMovement.camScroll = true;
        }

        //если главная панель закрыта, то все тултипы итемов в инвентаре убираем
        if (!generaPanelIsActive && FindObjectOfType<TooltipManager>())
        {
            Destroy(FindObjectOfType<TooltipManager>().gameObject);
        }

        //реакция на кнопку Escape
        if (Input.GetKeyDown(KeyCode.Escape))//при нажатии на кнопку 
        {
            if (generaPanelIsActive == true)//если открыта главная панель
            {
                generalPanel.SetActive(false);//закрываем ее
                generaPanelIsActive = false;//и помечаем как закрытую
            }
            else
            {
                if (pausePanelIsActive == false)//если пауза еще не активна
                {
                    Time.timeScale = 0.0f;//останавливаем время в игре
                    pausePanel.SetActive(true);//активируем панель паузы
                    pausePanelIsActive = true;//и помечаем ее как открытую
                }
                else if ((pausePanelIsActive == true) && (optionsPanelIsActive = false))
                {
                    pausePanel.SetActive(false);//деактивируем панель паузы
                    pausePanelIsActive = false;//и помечаем ее как закрытую
                    Time.timeScale = 1.0f;//запускаем время в игре
                }
                else if ((pausePanelIsActive == true) && (optionsPanelIsActive = true))
                {
                }

            }
        }

        //привязываем открытие/закрытие панели инвентаря к кнопке I
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (generaPanelIsActive == false)
            {
                OpenInventory();
            }
            else if (generaPanelIsActive == true && equipmentPanelIsActive == true)
            {
                generalPanel.SetActive(false);
                equipmentPanel.SetActive(false);
                generaPanelIsActive = false;
                equipmentPanelIsActive = false;
            }
            else if (generaPanelIsActive == true && equipmentPanelIsActive == false)
            {
                equipmentPanel.SetActive(true);
                inventory.UpdateCellsAmount();
                equipmentPanelIsActive = true;
            }
        }
    }

    //обновляем инфу о членах группы (когда добавляем нового мембера)
    public void AddMemberInfo()
    {
        for (int i = 0; i < characterPanels.Length; i++)
        {
            if (characterPanels[i] == null)
            {
                //создаем панель персонажа
                characterPanel = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/UI Elements/Character Panel"));
                characterPanel.transform.SetParent(groupPanel.transform, false);
                characterPanels[i] = characterPanel;

                //сообщаем компонентам, инфу какого персонажа они должны отображать
                characterPanel.GetComponentInChildren<HealthSliderManager>().character = group.members[i];
                characterPanel.GetComponentInChildren<AvatarPanelManager>().characterAvatar = group.members[i].GetComponent<CharacterProfile>().CharacterAvatar;
                characterPanel.GetComponentInChildren<AvatarPanelManager>().character = group.members[i];

                //обновляем аватары в панели снаряжения
                UpdateInnerAvatars();

                break;
            }
        }
    }

    //кнопка возврата в игру из окна паузы
    public void BackToGame()
    {
        Time.timeScale = 1.0f;//запускаем время в игре
    }
    //кнопка "выйти в главное меню"
    public void LoadingTheMainMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    //кнопка "выход из игры"
    public void Exit()
    {
        Application.Quit();
    }
    //кнопка "открыть инвентарь" (рюкзачок)
    public void OpenInventory()
    {
        generalPanel.SetActive(true);
        equipmentPanel.SetActive(true);
        if (inventory == null)
        {
            inventory = FindObjectOfType<InventoryPanelManager>();
        }
        inventory.UpdateCellsAmount();
        generaPanelIsActive = true;
        equipmentPanelIsActive = true;
    }

    //кнопка "открыть панель настроек"
    public void OpenOptions()
    {
        optionsPanel.SetActive(true);
        optionsPanelIsActive = true;
    }

      
    //аватары на панели снаряжения
    public void UpdateInnerAvatars()
    {
        for (int i = 0; i < group.groupMembersCount; i++)
        {
            if (innerAvatars[i] == null)
            {
                innerAvatars[i] = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/Avatars/" + group.members[i].GetComponent<CharacterProfile>().CharacterName));
                if (i == 0)
                {
                    innerAvatars[i].transform.SetParent(innerAvatarPanel0.transform, false);
                    innerAvatars[i].transform.SetAsFirstSibling();
                }
                else if (i == 1)
                {
                    innerAvatarPanel1.SetActive(true);
                    innerAvatars[i].transform.SetParent(innerAvatarPanel1.transform, false);
                    innerAvatars[i].transform.SetAsFirstSibling();
                }
                else if (i == 2)
                {
                    innerAvatarPanel2.SetActive(true);
                    innerAvatars[i].transform.SetParent(innerAvatarPanel2.transform, false);
                    innerAvatars[i].transform.SetAsFirstSibling();
                }
                else if (i == 3)
                {
                    innerAvatarPanel3.SetActive(true);
                    innerAvatars[i].transform.SetParent(innerAvatarPanel3.transform, false);
                    innerAvatars[i].transform.SetAsFirstSibling();
                }
            }
        }
    }


}
