﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//скрипт лейбла над головой непися
//висит на неписе

public class CharacterLabelManager : MonoBehaviour
{

    public GameObject panel;//родительская панель лейбла
    public GameObject theObject;//текущий объект
    public GameObject nameLabel;//лейбл
    public CharacterProfile NPCProfile; //профиль объекта
    public string NPCName;//имя объекта

    void Start()
    {
        theObject = this.gameObject;
        NPCProfile = theObject.GetComponent<CharacterProfile>();
        NPCProfile.ApplyLabelName();
        NPCName = NPCProfile.CharacterLabelName;
        nameLabel = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/UI Elements/Name Label"));//инстанцируем лейбл
        nameLabel.transform.SetParent(panel.transform, false);//и помещаем его на панель
        nameLabel.GetComponentInChildren<Text>().text = NPCName;//меняем текст на лейбле
    }

    void Update()
    {
        Vector2 position = Camera.main.WorldToScreenPoint(theObject.transform.position);//получаем позицию объекта в экранных координатах
        position.y += 48f;//поднимаем ее над головой персонажа
        nameLabel.transform.position = position;//помещаем лейбл в эти координаты
    }

    //уничтожение лейбла 
    public void DestroyLabel()
    {
        Destroy(nameLabel);

    }
}
