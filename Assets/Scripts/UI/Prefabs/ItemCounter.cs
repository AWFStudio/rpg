﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//этот скрипт отображает количество предметов в ячейке
//висит на иконке айтема


public class ItemCounter : MonoBehaviour
{

    public int quantity;

    private void Update()
    {
        quantity = GetComponentInParent<ItemComponent>().item.InCell;

        if (quantity > 1)
        {
            GetComponent<Text>().text = quantity.ToString();
        }
        else
        {
            GetComponent<Text>().text = null;

        }
    }

}
