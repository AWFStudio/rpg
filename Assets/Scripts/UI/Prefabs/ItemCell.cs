﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//скрипт ячейки под айтем
//висит на префабе ячейки
//используется в инвентаре, в куклах персонажей, в окне торговли, в контейнерах

public class ItemCell : MonoBehaviour
{
    public enum CellType
    {
        inventory,
        outfit,
        container,
        vendor
    }

    public enum OutfitCellType
    {
        head,
        pauldron,
        chest,
        legs,
        hands,
        feet,
        neck,
        ring,
        bag,
        belt,
        accessory,
        rightWeapon,
        leftWeapon,
        pocket
    }
    
    public CellType cellType;
    public OutfitCellType outfitCellType;

}
