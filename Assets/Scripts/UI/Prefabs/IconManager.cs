﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;//подключаем, чтобы иметь возможность перетаскивать иконки инвентаря мышкой

//этот скрипт нужен, чтобы:
//1. перетаскивать иконку предмета по инвентарю, снимать и надевать его на персонажа
//2. Удалять предмет из инвентаря, когда он заюзан
//висит на префабе иконки предмета

public class IconManager : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerClickHandler  //нужные интерфейсы не забываем 

{
    OutfitPanelManager outfit;//панель снаряжения
    InventoryPanelManager inventory;//панель инвентаря
    UIStatisticsPanelManager statistics;//панель статистики
    Transform canvas;//трансформ канваса
    GameObject tooltipPrefab;//префаб панели тултипа
    GameObject tooltip;//экземпляр панели тултипа
    GameObject temporarySlot;//слот, для временного хранения предмета, извлеченного из ячейки, в которую мы помещаем новый предмет
    bool drag;//чтобы проверять, перетаскивается ли иконка в настоящий момент
    bool isItemFromInventory;//чтобы проверять, является ли перетаскиваемая иконка предметом, надетым на персонажа
    DraggedItemType draggedItemType;//что за предмет перетаскиваем (если этот предмет был одет на персонажа)
    DraggedItemCellType draggedItemCellType;
    ItemCell[] itemCells;//массив всех ячеек (инвентаря и экипировки)

    enum DraggedItemType
    {
        none,
        rightWeapon,
        leftWeapon
    }

    enum DraggedItemCellType
    {
        none,
        itemFromOutfit,
        itemFromInventory
    }

    ItemCell cell;//родительская ячейка айтема
    public ItemCell Cell { get => cell; set => cell = value; }

    public void Start()
    {
        inventory = FindObjectOfType<InventoryPanelManager>();
        outfit = FindObjectOfType<OutfitPanelManager>();
        statistics = FindObjectOfType<UIStatisticsPanelManager>();
        canvas = GameObject.Find("Main IU Canvas").transform;
        tooltipPrefab = Resources.Load<GameObject>("Prefabs/UIPrefabs/UI Elements/Item Tooltip Panel");
        Cell = GetComponentInParent<ItemCell>();//получаем ссылку на родительскую ячейку
        itemCells = FindObjectsOfType<ItemCell>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        //если это иконка сумки, надетой на персонажа и если это единственная сумка в пати, то мы запрещаем ее перемещать
        if (GetComponent<ItemComponent>().item.ItemAppointment == Item.Item_Appointment.bag
            && outfit.character.GetComponent<CharacterEquipment>().bag.BagCellsAmount == inventory.InventoryCapacity)
        {
            return;
        }
        //в любом другом случае
        else
        {
            //если перетаскиваемая иконка является предметом, одетым на персонажа, отмечаем это
            if (Cell.cellType == ItemCell.CellType.outfit)
            {
                draggedItemCellType = DraggedItemCellType.itemFromOutfit;
                //также отмечаем, что это за предмет экипировки
                if (Cell.outfitCellType == ItemCell.OutfitCellType.rightWeapon)
                {
                    draggedItemType = DraggedItemType.rightWeapon;
                }
                else if (Cell.outfitCellType == ItemCell.OutfitCellType.leftWeapon)
                {
                    draggedItemType = DraggedItemType.leftWeapon;
                }

            }
            else if (Cell.cellType == ItemCell.CellType.inventory)
            {
                draggedItemCellType = DraggedItemCellType.itemFromInventory;
            }
            transform.SetParent(canvas);//делаем иконку дочерним объектом канваса
            transform.position = eventData.position;//перемещаем иконку за курсором
            drag = true;
            if (tooltip)//если включен тултип
            {
                DestroyTooltip();
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    //когда мы отпускаем иконку, она "падает" в ближайшую ячейку (если эта ячейка подходит по типу)
    {
        //если это иконка сумки, надетой на персонажа и если это единственная сумка в пати, то мы запрещаем ее перемещать
        if (GetComponent<ItemComponent>().item.ItemAppointment == Item.Item_Appointment.bag
            && outfit.character.GetComponent<CharacterEquipment>().bag.BagCellsAmount == inventory.InventoryCapacity)
        {
            return;
        }

        float distance = float.MaxValue;

        ItemCell newCell = Cell;//если мы не найдем другую ячейку, то вернем в ту же, откуда взяли
        ItemCell oldCell = Cell;//если мы захотим положить предмет в ячейку, где уже что-то лежит, то предмет оттуда переместим в нашу старую

        for (int i = 0; i < itemCells.Length; i++)//пробегаемся по всем ячейкам инвентаря и снаряжения
        {
            //измеряем расстояние от иконки до итой ячейки и помещаем значение во временную переменную
            float temp = Vector2.Distance(transform.position, itemCells[i].transform.position);

            if (temp < distance//сравниваем расстояние до нашей ячейки с расстоянием до итой ячейки, и, если итая ячейка ближе...
                && itemCells[i].isActiveAndEnabled)// и активна, то делаем ее текущей
            {
                distance = temp;
                newCell = itemCells[i];
            }
        }

        //если это был предмет с куклы
        if (draggedItemCellType == DraggedItemCellType.itemFromOutfit)
        {
            //и мы пытаемся поместить его в ячейку инвентаря
            if (newCell.cellType == ItemCell.CellType.inventory)
            {
                //если нашлась пустая ячейка в инвентаре, то помещаем предмет в нее
                if (inventory.AddNewItem(GetComponent<ItemComponent>().item))
                {
                    //обнуляем поле этого предмета в скрипте снаряжения персонажа
                    if (draggedItemType == DraggedItemType.rightWeapon)
                    {
                        outfit.character.GetComponent<CharacterEquipment>().rightWeapon = null;
                    }
                    else if (draggedItemType == DraggedItemType.leftWeapon)
                    {
                        outfit.character.GetComponent<CharacterEquipment>().leftWeapon = null;
                    }
                    Destroy(gameObject);//удаляем иконку с курсора
                    statistics.UpdateDamageStatistics();//обновляем статистику персонажа
                }
                //а если в инвентаре свободной ячейки нет, то возвращаем перекладываемый предмет обратно в "родную" ячейку
                else
                {
                    ReturnThisItemBack();
                }
            }
            //и мы пытаемся переместить его в ячейку снаряжения
            else if (newCell.cellType == ItemCell.CellType.outfit)
            {
                //если это было оружие из правой руки 
                if (draggedItemType == DraggedItemType.rightWeapon)
                {
                    //если выбранная ячейка не предназначена для оружия, то возвращаем предмет обратно в ту ячейку, из которой взяли
                    if (newCell.outfitCellType != ItemCell.OutfitCellType.leftWeapon)
                    {
                        ReturnThisItemBack();
                    }
                    //если это ячейка левой руки... 
                    if (newCell.outfitCellType == ItemCell.OutfitCellType.leftWeapon)
                    {
                        //и оно двуручное/праворучное (т.е. не может быть помещено в ячейку левой руки)
                        if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon.WeaponType == Weapon.Weapon_Type.twoHanded
                            || outfit.character.GetComponent<CharacterEquipment>().rightWeapon.WeaponType == Weapon.Weapon_Type.rightHanded)
                        {
                            //то возвращаем его обратно
                            ReturnThisItemBack();
                        }
                        //иначе проверяем, пуста ли целевая ячейка
                        else
                        {
                            //и если в ней что-то лежит
                            if (newCell.transform.childCount >= 1)
                            {
                                //проверяем, можно ли это что-то положить в ячейку правой руки
                                if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon.WeaponType == Weapon.Weapon_Type.bothHanded)
                                {
                                    //если да, то меняем предметы местами
                                    GameObject thisItem = newCell.transform.GetChild(0).gameObject as GameObject;//и "перекладываем" в нее то, что лежит в ячейке, в которую мы хотим поместить перетаскиваемый айтем
                                    PutThisItemToOldSell(thisItem);
                                    PutDraggedItemInNewCell();
                                    outfit.character.GetComponent<CharacterEquipment>().leftWeapon = (Weapon)GetComponent<ItemComponent>().item;
                                    outfit.character.GetComponent<CharacterEquipment>().rightWeapon = (Weapon)temporarySlot.GetComponent<ItemComponent>().item;
                                    statistics.UpdateDamageStatistics();
                                }
                                //а если нельзя
                                else
                                {
                                    //если нашлась пустая ячейка в инвентаре, то предмет из левой руки перекладываем в инвентарь
                                    if (inventory.AddNewItem(newCell.GetComponentInChildren<ItemComponent>().item))
                                    {
                                        temporarySlot = newCell.transform.GetChild(0).gameObject as GameObject;
                                        Destroy(newCell.transform.GetChild(0).GetComponent<ItemComponent>().item);
                                        outfit.character.GetComponent<CharacterEquipment>().leftWeapon = (Weapon)temporarySlot.GetComponent<ItemComponent>().item;
                                        outfit.character.GetComponent<CharacterEquipment>().rightWeapon = null;
                                        PutDraggedItemInNewCell();
                                        statistics.UpdateDamageStatistics();
                                    }
                                    //а если в инвентаре свободной ячейки нет, то возвращаем перекладываемый предмет обратно в "родную" ячейку
                                    else
                                    {
                                        ReturnThisItemBack();
                                    }
                                }
                            }
                            //а если она пуста, то просто перекладываем предмет в нее
                            else
                            {
                                outfit.character.GetComponent<CharacterEquipment>().leftWeapon = (Weapon)GetComponent<ItemComponent>().item;
                                outfit.character.GetComponent<CharacterEquipment>().rightWeapon = null;
                                PutDraggedItemInNewCell();
                                statistics.UpdateDamageStatistics();
                            }
                        }
                    }
                }
                //если это было оружие из левой руки 
                else if (draggedItemType == DraggedItemType.leftWeapon)
                {
                    //если выбранная ячейка не предназначена для оружия, то возвращаем предмет обратно в ту ячейку, из которой взяли
                    if (newCell.outfitCellType != ItemCell.OutfitCellType.rightWeapon)
                    {
                        ReturnThisItemBack();
                    }
                    //иначе
                    else if (newCell.outfitCellType == ItemCell.OutfitCellType.rightWeapon)
                    {
                        //если перетаскиваемый предмет нельзя поместить в правую руку
                        if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon.WeaponType == Weapon.Weapon_Type.leftHanded)
                        {
                            ReturnThisItemBack();
                        }
                        //иначе проверяем, пуста ли целевая ячейка
                        else
                        {
                            //и если в ней что-то лежит
                            if (newCell.transform.childCount >= 1)
                            {
                                //проверяем, можно ли это что-то положить в ячейку левой руки
                                if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon.WeaponType == Weapon.Weapon_Type.bothHanded)
                                {
                                    //если да, то меняем предметы местами
                                    GameObject thisItem = newCell.transform.GetChild(0).gameObject as GameObject;//и "перекладываем" в нее то, что лежит в ячейке, в которую мы хотим поместить перетаскиваемый айтем
                                    PutThisItemToOldSell(thisItem);
                                    PutDraggedItemInNewCell();
                                    outfit.character.GetComponent<CharacterEquipment>().rightWeapon = (Weapon)GetComponent<ItemComponent>().item;
                                    outfit.character.GetComponent<CharacterEquipment>().leftWeapon = (Weapon)temporarySlot.GetComponent<ItemComponent>().item;
                                    statistics.UpdateDamageStatistics();
                                }
                                //а если нельзя
                                else
                                {
                                    //если нашлась пустая ячейка в инвентаре, то предмет из правой руки перекладываем в инвентарь
                                    if (inventory.AddNewItem(newCell.GetComponentInChildren<ItemComponent>().item))
                                    {
                                        temporarySlot = newCell.transform.GetChild(0).gameObject as GameObject;
                                        Destroy(newCell.transform.GetChild(0).GetComponent<ItemComponent>().item);
                                        outfit.character.GetComponent<CharacterEquipment>().rightWeapon = (Weapon)temporarySlot.GetComponent<ItemComponent>().item;
                                        outfit.character.GetComponent<CharacterEquipment>().leftWeapon = null;
                                        PutDraggedItemInNewCell();
                                        statistics.UpdateDamageStatistics();
                                    }
                                    //а если в инвентаре свободной ячейки нет, то возвращаем перекладываемый предмет обратно в "родную" ячейку
                                    else
                                    {
                                        ReturnThisItemBack();
                                    }
                                }
                            }
                            //а если она пуста, то просто перекладываем предмет в нее
                            else
                            {
                                outfit.character.GetComponent<CharacterEquipment>().rightWeapon = (Weapon)GetComponent<ItemComponent>().item;
                                outfit.character.GetComponent<CharacterEquipment>().leftWeapon = null;
                                PutDraggedItemInNewCell();
                                statistics.UpdateDamageStatistics();
                            }
                        }
                    }
                }
            }
        }
        //если это был предмет из инвентаря
        else if (draggedItemCellType == DraggedItemCellType.itemFromInventory)
        {
            //если мы перетаскиваем его по инвентарю
            if (newCell.cellType == ItemCell.CellType.inventory)
            {
                if (newCell.transform.childCount >= 1)//если в ячейке уже что-то лежит
                {
                    GameObject thisItem = newCell.transform.GetChild(0).gameObject as GameObject;//и "перекладываем" в нее то, что лежит в ячейке, в которую мы хотим поместить перетаскиваемый айтем
                    PutThisItemToOldSell(thisItem);
                }
                PutDraggedItemInNewCell();
            }
            //если мы пытаемся надеть его на персонажа
            else if (newCell.cellType == ItemCell.CellType.outfit)
            {
                if (GetComponent<ItemComponent>().item.ItemAppointment == Item.Item_Appointment.weapon)//и если это оружие
                {
                    Weapon thisWeapon = (Weapon)GetComponent<ItemComponent>().item;
                    //если это двуруч
                    if (thisWeapon.WeaponClass == Weapon.Weapon_Class.longSword || thisWeapon.WeaponClass == Weapon.Weapon_Class.sword)
                    {
                        //если мы пытаемся надеть его не на Валькара
                        if (outfit.character.GetComponent<CharacterProfile>().name != "Valkar")
                        {
                            //возвращаем предмет в исходную ячейку
                            ReturnThisItemBack();
                        }
                        else
                        {
                            EquipAnWeapon(thisWeapon);
                        }
                    }
                }
            }
        }

        inventory.UpdateGroupWeight();

        //метод для возвращения айтема в исходную ячейку, если новая не удовлетворяет каким-либо требованиям
        void ReturnThisItemBack()
        {
            Cell = oldCell;//помещаем выбранную ячейку в переменную родительской ячейки
            transform.SetParent(Cell.transform);//переназначаем ее в качестве родителя
            transform.position = Cell.transform.position;//и помещаем в нее объект
            drag = false;
            draggedItemType = DraggedItemType.none;
            draggedItemCellType = DraggedItemCellType.none;
        }

        //метод для помещения перетаскиваемого айтема в новую ячейку
        void PutDraggedItemInNewCell()
        {
            Cell = newCell;//помещаем выбранную ячейку в переменную родительской ячейки
            transform.SetParent(Cell.transform);//переназначаем ее в качестве родителя
            transform.position = Cell.transform.position;//и помещаем в нее объект
            drag = false;
        }

        //метод для перекладывания айтема, лежащего в целевой ячейке, в исходную ячейку того айтема, который мы помещаем в новую
        void PutThisItemToOldSell(GameObject thisItem)
        {
            temporarySlot = thisItem;//и "перекладываем" в нее то, что лежит в ячейке, в которую мы хотим поместить перетаскиваемый айтем
            temporarySlot.transform.SetParent(oldCell.transform);//затем делаем ее "дочкой" бывшей ячейки перетаскиваемого айтема
            temporarySlot.transform.position = oldCell.transform.position;//и выравниваем по ней
            temporarySlot.GetComponent<IconManager>().Cell = oldCell;//не забываем в менеджере этого айтема указать, что у него теперь другая родительская ячейка
        }

        //метод для перекладывания предмета из левой руки в инвентарь (когда мы помещаем в правую руку двуручное оружие)
        bool PutItemFromLeftHandInInventory()
        {
            Item leftWeapon = outfit.character.GetComponent<CharacterEquipment>().leftWeapon;
            //если в инвентаре нашлась свободная ячейка
            if (inventory.AddNewItem(leftWeapon))
            {
                outfit.character.GetComponent<CharacterEquipment>().leftWeapon = null;
                outfit.UpdateOutfit();
                statistics.UpdateDamageStatistics();
                return true;
            }
            //если в инвентаре свободной ячейки нет
            return false;
        }

        //метод для замены предмета, находящегося в правой руке персонажа, на перетаскиваемый сейчас
        void ChangeItemInRightHandWithThis()
        {
            //пробегаемся по всем ячейкам
            for (int i = 0; i < itemCells.Length; i++)
            {
                //выбираем ячейки куклы
                if (itemCells[i].cellType == ItemCell.CellType.outfit)
                {
                    //среди них находим ячейку правой руки
                    if (itemCells[i].outfitCellType == ItemCell.OutfitCellType.rightWeapon)
                    {
                        //перекладываем предмет из правой руки в нашу старую ячейку
                        GameObject thisItem = itemCells[i].GetComponentInChildren<ItemComponent>().gameObject;
                        PutThisItemToOldSell(thisItem);
                        //помечаем ее, как целевую
                        newCell = itemCells[i];
                        //помещаем перетаскиваемый предмет в нее
                        PutDraggedItemInNewCell();
                        //помещаем этот предмет в скрипт снаряжения
                        Weapon thisWeapon = (Weapon)GetComponent<ItemComponent>().item;
                        outfit.character.GetComponent<CharacterEquipment>().rightWeapon = thisWeapon;
                        //обновляем статистику
                        statistics.UpdateDamageStatistics();
                        return;
                    }
                }
            }
        }

        //метод для помещения перетаскиваемого предмета в правую руку, если она пуста
        void PutThisItemInTheRightHand()
        {
            //пробегаемся по всем ячейкам
            for (int i = 0; i < itemCells.Length; i++)
            {
                //выбираем ячейки куклы
                if (itemCells[i].cellType == ItemCell.CellType.outfit)
                {
                    //среди них находим ячейку правой руки
                    if (itemCells[i].outfitCellType == ItemCell.OutfitCellType.rightWeapon)
                    {
                        //помечаем ее, как целевую
                        newCell = itemCells[i];
                        //помещаем перетаскиваемый предмет в нее
                        PutDraggedItemInNewCell();
                        //помещаем этот предмет в скрипт снаряжения
                        Weapon thisWeapon = (Weapon)GetComponent<ItemComponent>().item;
                        outfit.character.GetComponent<CharacterEquipment>().rightWeapon = thisWeapon;
                        //обновляем статистику
                        statistics.UpdateDamageStatistics();
                        return;
                    }
                }
            }

        }

        //метод для помещения перетаскиваемого предмета в левую руку, если она пуста
        void PutThisItemInTheLeftHand()
        {
            //пробегаемся по всем ячейкам
            for (int i = 0; i < itemCells.Length; i++)
            {
                //выбираем ячейки куклы
                if (itemCells[i].cellType == ItemCell.CellType.outfit)
                {
                    //среди них находим ячейку левой руки
                    if (itemCells[i].outfitCellType == ItemCell.OutfitCellType.leftWeapon)
                    {
                        //помечаем ее, как целевую
                        newCell = itemCells[i];
                        //помещаем перетаскиваемый предмет в нее
                        PutDraggedItemInNewCell();
                        //помещаем этот предмет в скрипт снаряжения
                        Weapon thisWeapon = (Weapon)GetComponent<ItemComponent>().item;
                        outfit.character.GetComponent<CharacterEquipment>().leftWeapon = thisWeapon;
                        //обновляем статистику
                        statistics.UpdateDamageStatistics();
                        return;
                    }
                }
            }

        }

        //метод для надевания оружия на персонажа
        void EquipAnWeapon(Weapon thisWeapon)
        {
            //если это двуручное оружие
            if (thisWeapon.WeaponType == Weapon.Weapon_Type.twoHanded)
            {
                //если руки персонажа пусты
                if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon == null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon == null)
                {
                    PutThisItemInTheRightHand();
                }
                //если левая рука пуста, а в правой что-то есть
                else if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon == null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null)
                {
                    ChangeItemInRightHandWithThis();
                }
                //если правая рука пуста, а в левой что-то есть
                else if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon != null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon == null)
                {
                    //пробегаемся по всем ячейкам
                    for (int i = 0; i < itemCells.Length; i++)
                    {
                        //выбираем ячейки куклы
                        if (itemCells[i].cellType == ItemCell.CellType.outfit)
                        {
                            //среди них находим ячейку левой руки
                            if (itemCells[i].outfitCellType == ItemCell.OutfitCellType.leftWeapon)
                            {
                                //перекладываем предмет из нее в нашу старую ячейку
                                GameObject thisItem = itemCells[i].GetComponentInChildren<ItemComponent>().gameObject;
                                PutThisItemToOldSell(thisItem);
                                outfit.character.GetComponent<CharacterEquipment>().leftWeapon = null;
                                PutThisItemInTheRightHand();
                                statistics.UpdateDamageStatistics();
                                return;
                            }
                        }
                    }
                }
                //если в обеих руках что-то есть
                else if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon != null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null)
                {
                    ChangeItemInRightHandWithThis();
                    PutItemFromLeftHandInInventory();
                }
            }
            //если это оружие для любой руки
            else if (thisWeapon.WeaponType == Weapon.Weapon_Type.bothHanded)
            {
                //если руки персонажа пусты
                if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon == null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon == null)
                {
                    PutThisItemInTheRightHand();
                }
                //если левая рука пуста, а в правой что-то есть
                else if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon == null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null)
                {
                    if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon.WeaponType != Weapon.Weapon_Type.twoHanded)
                    {
                        PutThisItemInTheLeftHand();
                    }
                    else
                    {
                        ChangeItemInRightHandWithThis();
                    }
                }
                //если правая рука пуста, а в левой что-то есть
                else if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon != null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon == null)
                {
                    PutThisItemInTheRightHand();
                }
                //если в обеих руках что-то есть
                else if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon != null && outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null)
                {
                    ChangeItemInRightHandWithThis();
                }
            }

        }
    }

    //метод для использования предмета из инвентаря
    public void OnPointerClick(PointerEventData eventData)
    {
        if (Input.GetMouseButtonUp(1))//если мы кликнули правой кнопкой по иконке, проверяем, сколько предметов в ячейке
        {
            if (gameObject.GetComponent<Item>().InCell > 1)//если больше одного
            {
                gameObject.GetComponent<Item>().InCell -= 1;//минусуем один
            }
            else if (gameObject.GetComponent<Item>().InCell == 1)//если один
            {
                gameObject.GetComponent<Item>().InCell -= 1;//минусуем один

                if (tooltip)//проверяем наличие тултипа
                {
                    Destroy(tooltip);//удаляем тултип
                }
                Destroy(gameObject);//и удаляем предмет
            }
        }
    }

    //метод для удаления тултипа. 
    public void DestroyTooltip()
    {
        Destroy(tooltip);
    }
}
