﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//скрипт тултипа элемента интерфейса
//висит на элементе, к которому надо выводить тултип

public class TooltipManager : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDragHandler
{
    //тип тултипа
    public enum TypeOfTooltip
    {
        text,
        inventoryItem,
        damageStatistics
    }

    OutfitPanelManager outfit;//панель куклы
    GameObject UItooltip;//панель тултипа
    GameObject UITooltipPanelPref;//префаб панели тултипа
    Transform canvas;//канвас
    TextAsset asset1;//текст-ассет
    TextAsset asset2;//текст-ассет
    XMLSettings UIelement;
    XMLSettings InventoryItem;

    [SerializeField] TypeOfTooltip tooltipType;//тип тултипа
    [SerializeField] int uIelementID = 0;//индекс текста в массиве тултипов (для тултипов типа "текст")
    Text tooltipText;//текст тултипа
    int itemID = 0;//индекс айтема в массиве тултипов (для тултипов к айтемам)


    public int UIelementID { get => uIelementID; set => uIelementID = value; }
    public TypeOfTooltip TooltipType { get => tooltipType; set => tooltipType = value; }

    public void Awake()
    {
        outfit = FindObjectOfType<OutfitPanelManager>();
        UITooltipPanelPref = Resources.Load<GameObject>("Prefabs/UIPrefabs/UI Elements/UI Tooltip Panel");
    }

    public void Update()
    {
        if (UItooltip)
        {
            //позиционируем тултип, относительно текущего положения курсора
            if (((int)Input.mousePosition.x > 0) && ((int)Input.mousePosition.x < Screen.width / 2) && ((int)Input.mousePosition.y > 0) && ((int)Input.mousePosition.y < Screen.height / 2))
            {
                UItooltip.transform.position = new Vector3(Input.mousePosition.x + (UItooltip.GetComponent<RectTransform>().rect.width / 2 + 20), Input.mousePosition.y + (UItooltip.GetComponent<RectTransform>().rect.height / 2), transform.position.z);
            }
            if (((int)Input.mousePosition.x > Screen.width / 2) && ((int)Input.mousePosition.y > 0) && ((int)Input.mousePosition.y < Screen.height / 2))
            {
                UItooltip.transform.position = new Vector3(Input.mousePosition.x - (UItooltip.GetComponent<RectTransform>().rect.width / 2 + 20), Input.mousePosition.y + (UItooltip.GetComponent<RectTransform>().rect.height / 2), transform.position.z);
            }
            if (((int)Input.mousePosition.x > 0) && ((int)Input.mousePosition.x < Screen.width / 2) && ((int)Input.mousePosition.y > Screen.height / 2))
            {
                UItooltip.transform.position = new Vector3(Input.mousePosition.x + (UItooltip.GetComponent<RectTransform>().rect.width / 2 + 20), Input.mousePosition.y - (UItooltip.GetComponent<RectTransform>().rect.height / 2), transform.position.z);
            }
            if ((int)Input.mousePosition.x > Screen.width / 2 && ((int)Input.mousePosition.y > Screen.height / 2))
            {
                UItooltip.transform.position = new Vector3(Input.mousePosition.x - (UItooltip.GetComponent<RectTransform>().rect.width / 2 + 20), Input.mousePosition.y - (UItooltip.GetComponent<RectTransform>().rect.height / 2), transform.position.z);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)//когда курсор находится над элементом
    {
        if (!UItooltip)//если тултип еще не открыт
        {
            if (GameObject.Find("Canvas") != null)
            {
                canvas = GameObject.Find("Canvas").transform;//находим трансформ канваса
            }
            else if (GameObject.Find("Main IU Canvas") != null)
            {
                canvas = GameObject.Find("Main IU Canvas").transform;//находим трансформ канваса
            }
            UItooltip = Instantiate(UITooltipPanelPref) as GameObject;//вызываем панель тултипа
            UItooltip.transform.SetParent(canvas);//затем перемещаем на канвас, чтобы она оказалась поверх всех окон
            ShowTooltipContent();
        }
    }
    //когда уводим курсор с объекта
    public void OnPointerExit(PointerEventData eventData)
    {
        if (UItooltip)//если включен тултип
        {
            Destroy(UItooltip);//удаляем его
        }
    }
    //когда перетаскиваем объект
    public void OnDrag(PointerEventData eventData)
    {
        if (UItooltip)//если включен тултип
        {
            Destroy(UItooltip);//удаляем его
        }
    }

    public void ShowTooltipContent()
    {
        //тип тултипа - текст
        if (TooltipType == TypeOfTooltip.text)
        {
            asset1 = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/UI Tooltips");//находим ассет
            UIelement = XMLSettings.Load(asset1);//выгружаем его

            //включаем панель и выводим текст
            GameObject tooltip = UItooltip.transform.GetChild(0).gameObject;
            tooltip.SetActive(true);
            tooltip.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[uIelementID].text;
        }

        //тип тултипа - статистика урона
        else if (TooltipType == TypeOfTooltip.damageStatistics)
        {
            asset1 = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Statistics Tooltips");//находим ассет
            UIelement = XMLSettings.Load(asset1);//выгружаем его

            //включаем панель и выводим текст
            GameObject tooltip = UItooltip.transform.GetChild(0).gameObject;
            tooltip.SetActive(true);
            tooltip.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[uIelementID].text;

            //включаем заголовок и выводим его текст
            GameObject headline = UItooltip.transform.GetChild(1).gameObject;
            headline.SetActive(true);
            headline.transform.GetChild(2).gameObject.SetActive(true);
            headline.transform.GetChild(2).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].headline;

            ShowWeaponBonus();
            ShowDamageBonus();
        }
        //тип тултипа - предмет
        else if (TooltipType == TypeOfTooltip.inventoryItem)
        {
            asset1 = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Inventory Items Tooltips");//находим ассет
            InventoryItem = XMLSettings.Load(asset1);//выгружаем его
            asset2 = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");//находим ассет
            UIelement = XMLSettings.Load(asset2);//выгружаем его

            //отключаем базовый текст тултипа
            GameObject text = UItooltip.transform.GetChild(0).gameObject;
            text.SetActive(false);

            itemID = GetComponent<ItemComponent>().item.ItemID;

            //если в стаке несколько предметов
            if (GetComponentInChildren<ItemCounter>().quantity > 1)
            {
                Debug.Log("quantity > 1");
                //включаем заголовок №0 (выровнен по правому краю) и выводим его текст
                GameObject headline = UItooltip.transform.GetChild(1).gameObject;
                headline.SetActive(true);
                headline.transform.GetChild(0).gameObject.SetActive(true);
                headline.transform.GetChild(0).gameObject.GetComponent<Text>().text = InventoryItem.InventoryItems[itemID].name;

                //включаем счетчик и отображаем количество предметов
                headline.transform.GetChild(1).gameObject.SetActive(true);
                headline.transform.GetChild(1).gameObject.GetComponent<Text>().text = "(" + GetComponent<ItemCounter>().quantity.ToString() + ")";
            }
            //иначе
            else
            {
                //включаем заголовок №3 (выровнен по центру) и выводим его текст
                GameObject headline = UItooltip.transform.GetChild(1).gameObject;
                headline.SetActive(true);
                headline.transform.GetChild(2).gameObject.SetActive(true);
                headline.transform.GetChild(2).gameObject.GetComponent<Text>().text = InventoryItem.InventoryItems[itemID].name;
            }

            //включаем тип предмета и выводим его текст
            GameObject type = UItooltip.transform.GetChild(2).gameObject;
            type.SetActive(true);
            type.transform.GetChild(0).gameObject.GetComponent<Text>().text = InventoryItem.InventoryItems[itemID].type;

            //если это оружие
            if (GetComponent<ItemComponent>().item.ItemAppointment == Item.Item_Appointment.weapon)
            {
                Weapon thisWeapon = (Weapon)GetComponent<ItemComponent>().item;

                //включаем панель и отображаем урон
                GameObject damage = UItooltip.transform.GetChild(3).gameObject;
                damage.SetActive(true);
                damage.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[70].text;
                damage.transform.GetChild(1).gameObject.GetComponent<Text>().text = thisWeapon.MinDamage.ToString() + "-" + thisWeapon.MaxDamage.ToString();

                //включаем панель и отображаем требуемый уровень
                GameObject level = UItooltip.transform.GetChild(4).gameObject;
                level.SetActive(true);
                level.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[72].text;
                level.transform.GetChild(1).gameObject.GetComponent<Text>().text = thisWeapon.WeaponLevel.ToString();

                //включаем панель и отображаем дальность
                GameObject range = UItooltip.transform.GetChild(5).gameObject;
                range.SetActive(true);
                range.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[73].text;
                range.transform.GetChild(1).gameObject.GetComponent<Text>().text = thisWeapon.Range.ToString();

                //если этот предмет - ржавый
                if (thisWeapon.WeaponCondition == Weapon.Weapon_Condition.rusted)
                {
                    //включаем панель и отображаем шанс сломать предмет при атаке
                    GameObject chanceToBreak = UItooltip.transform.GetChild(6).gameObject;
                    chanceToBreak.SetActive(true);
                    chanceToBreak.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[74].text;
                    chanceToBreak.transform.GetChild(1).gameObject.GetComponent<Text>().text = thisWeapon.ChanceToBreak.ToString() + "%";
                }

            }
            //если это сумка
            else if (GetComponent<ItemComponent>().item.ItemAppointment == Item.Item_Appointment.bag)
            {
                Bag thisBag = (Bag)GetComponent<ItemComponent>().item;
                int cellsCount = thisBag.BagCellsAmount;

                //включаем панель и отображаем количество ячеек
                GameObject cells = UItooltip.transform.GetChild(3).gameObject;
                cells.SetActive(true);
                cells.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[71].text;
                cells.transform.GetChild(1).gameObject.GetComponent<Text>().text = cellsCount.ToString();
            }

            //включаем описание и выводим его текст
            GameObject description = UItooltip.transform.GetChild(7).gameObject;
            description.SetActive(true);
            description.transform.GetChild(0).gameObject.GetComponent<Text>().text = InventoryItem.InventoryItems[itemID].description;

            //включаем панель и выводим вес и стоимость предмета/стака
            GameObject weightAndCost = UItooltip.transform.GetChild(8).gameObject;
            weightAndCost.SetActive(true);
            weightAndCost.transform.GetChild(1).gameObject.GetComponent<Text>().text = (GetComponent<ItemComponent>().item.ItemWeight * GetComponentInChildren<ItemCounter>().quantity).ToString();
            weightAndCost.transform.GetChild(3).gameObject.GetComponent<Text>().text = (GetComponent<ItemComponent>().item.ItemCost * GetComponentInChildren<ItemCounter>().quantity).ToString();


        }
    }

    public void ShowWeaponBonus()
    {
        //если в правой руке есть оружие
        if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null)
        {
            ShowrightWeaponBonus();
        }
        //если в левой руке есть оружие
        if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon != null)
        {
            ShowleftWeaponBonus();
        }
        //если обе руки пусты
        if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon == null && outfit.character.GetComponent<CharacterEquipment>().leftWeapon == null)
        {
            ShowEmptyHandsBonus();
        }
    }

    public void ShowEmptyHandsBonus()
    {
        GameObject modifier_01 = UItooltip.transform.GetChild(3).gameObject;
        modifier_01.SetActive(true);
        modifier_01.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_06;
        modifier_01.transform.GetChild(1).gameObject.GetComponent<Text>().text = "1-2";
    }

    public void ShowrightWeaponBonus()
    {
        GameObject modifier_01 = UItooltip.transform.GetChild(3).gameObject;
        modifier_01.SetActive(true);
        if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon.WeaponType != Weapon.Weapon_Type.twoHanded)
        {
            //если оружие двуручное
            modifier_01.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_01;
        }
        else
        {
            //если оружие одноручное
            modifier_01.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_03;
        }
        modifier_01.transform.GetChild(1).gameObject.GetComponent<Text>().text = outfit.character.GetComponent<CharacterEquipment>().rightWeapon.MinDamage.ToString() + "-" + outfit.character.GetComponent<CharacterEquipment>().rightWeapon.MaxDamage.ToString();
    }

    public void ShowleftWeaponBonus()
    {
        GameObject modifier_02 = UItooltip.transform.GetChild(4).gameObject;
        modifier_02.SetActive(true);
        modifier_02.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_02;
        if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon.WeaponType == Weapon.Weapon_Type.leftHanded)
        {
            //если оружие предназначено только для левой руки, отображаем урон "как есть"
            modifier_02.transform.GetChild(1).gameObject.GetComponent<Text>().text = outfit.character.GetComponent<CharacterEquipment>().leftWeapon.MinDamage.ToString() + "-" + outfit.character.GetComponent<CharacterEquipment>().leftWeapon.MaxDamage.ToString();
        }
        else if (outfit.character.GetComponent<CharacterEquipment>().leftWeapon.WeaponType == Weapon.Weapon_Type.bothHanded)
        {
            //если оружие универсальное, режем урон пополам
            modifier_02.transform.GetChild(1).gameObject.GetComponent<Text>().text = (outfit.character.GetComponent<CharacterEquipment>().leftWeapon.MinDamage / 2).ToString() + "-" + (outfit.character.GetComponent<CharacterEquipment>().leftWeapon.MaxDamage / 2).ToString();
        }
    }

    public void ShowDamageBonus()
    {
        //выводим модификатор силы или ловкости
        GameObject modifier_03 = UItooltip.transform.GetChild(5).gameObject;
        modifier_03.SetActive(true);
        //если в руках арбалет
        if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null
            && outfit.character.GetComponent<CharacterEquipment>().rightWeapon.WeaponClass == Weapon.Weapon_Class.crossbow)
        {
            modifier_03.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_05;
            modifier_03.transform.GetChild(1).gameObject.GetComponent<Text>().text = "+" + System.Convert.ToInt32(outfit.character.GetComponent<CharacterProfile>().DexMod * 1.5).ToString();
        }
        //если в руках двуруч
        else if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null
            && outfit.character.GetComponent<CharacterEquipment>().rightWeapon.WeaponClass == Weapon.Weapon_Class.longSword)
        {
            modifier_03.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_04;
            modifier_03.transform.GetChild(1).gameObject.GetComponent<Text>().text = "+" + System.Convert.ToInt32(outfit.character.GetComponent<CharacterProfile>().StrMod * 1.5).ToString();
        }
        //если в руках два оружия
        else if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon != null
            && outfit.character.GetComponent<CharacterEquipment>().leftWeapon != null)
        {
            modifier_03.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_04;
            modifier_03.transform.GetChild(1).gameObject.GetComponent<Text>().text = "+" + System.Convert.ToInt32(outfit.character.GetComponent<CharacterProfile>().StrMod * 1.5).ToString();

        }
        //если оружие только в левой руке
        else if (outfit.character.GetComponent<CharacterEquipment>().rightWeapon == null
            && outfit.character.GetComponent<CharacterEquipment>().leftWeapon != null)
        {
            modifier_03.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_04;
            modifier_03.transform.GetChild(1).gameObject.GetComponent<Text>().text = "+" + System.Convert.ToInt32(outfit.character.GetComponent<CharacterProfile>().StrMod * 0.5).ToString();
        }
        //в остальных случаях
        else
        {
            modifier_03.transform.GetChild(0).gameObject.GetComponent<Text>().text = UIelement.UIelements[0].mod_04;
            modifier_03.transform.GetChild(1).gameObject.GetComponent<Text>().text = "+" + outfit.character.GetComponent<CharacterProfile>().StrMod.ToString();
        }
    }

}
