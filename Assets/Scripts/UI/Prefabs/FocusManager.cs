﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//висит на префабе аватара мембера

public class FocusManager : MonoBehaviour, IPointerClickHandler
{

    public GameObject character;
    public GameObject frame;
    GroupManager group;
    GameManager game;

    public void Awake()
    {
        group = FindObjectOfType<GroupManager>();
        frame = Instantiate(Resources.Load<GameObject>("Prefabs/UIPrefabs/UI Elements/Frame"));
        frame.transform.SetParent(this.transform, false);
        game = FindObjectOfType<GameManager>();
    }

    public void Start()
    {
        if (group.groupMembersCount == 1)
        {
            frame.SetActive(true);
            character.GetComponent<CharacterProfile>().InFocus = true;
            game.UpdateSelectedMember();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Input.GetMouseButtonUp(0))
        {
            for (int i = 0; i < group.groupMembersCount; i++)
            {
                UIManager.characterPanels[i].GetComponentInChildren<FocusManager>().frame.SetActive(false);
                group.members[i].GetComponent<CharacterProfile>().InFocus = false;
            }
            frame.SetActive(true);
            character.GetComponent<CharacterProfile>().InFocus = true;
            game.UpdateSelectedMember();
        }
    }
}
