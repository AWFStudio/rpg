﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//этот скрипт отвечает за слайдер здоровья игрока
//висит на слайдере

public class HealthSliderManager : MonoBehaviour
{

    Slider slider;//слайдер
    public GameObject character;

    void Start()
    {
        slider = GetComponent<Slider>();//ссылаемся на слайдер, который висит на том же объекте, что и этот скрипт
    }

    void Update()
    {

        slider.value = character.GetComponent<CharacterProfile>().CurrentHealh;//значение слайдера = текущий % здоровья игрока

    }
}
