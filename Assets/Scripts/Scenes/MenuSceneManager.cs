﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//это скрипт главного меню
//висит на одноименном объекте на сцене меню

public class MenuSceneManager : MonoBehaviour
{

    TextAsset asset;
    XMLSettings textSettings;

    [SerializeField] Text newGame;
    [SerializeField] Text loadGame;
    [SerializeField] Text options;
    [SerializeField] Text creators;
    [SerializeField] Text exit;
    [SerializeField] Text continueGame;

    void Start()
    {
        ApplyLanguageSettings();
    }

    //новая игра
    public void NewGameButtonClick()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);//переходим к выбору персонажа
    }

    //выход
    public void Exit()
    {
        Application.Quit();
    }

    public void ApplyLanguageSettings()//локализация элементов UI
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Main Menu");
        textSettings = XMLSettings.Load(asset);

        //основное меню
        newGame.text = textSettings.UIelements[0].text;
        loadGame.text = textSettings.UIelements[1].text;
        options.text = textSettings.UIelements[2].text;
        creators.text = textSettings.UIelements[10].text;

        exit.text = textSettings.UIelements[3].text;
        continueGame.text = textSettings.UIelements[4].text;

    }


}
