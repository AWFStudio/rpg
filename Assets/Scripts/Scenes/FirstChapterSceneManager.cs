﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;//искин для работы навмеша

//этот скрипт отвечает за расстановку по своим местам ключевых персонажей в первой сцене, одного из которых к этому моменту выбрал для себя игрок
//и за вывод самого первого сообщения
//висит на пустом элементе

public class FirstChapterSceneManager : MonoBehaviour
{
    //точка появления персонажа игрока:
    public GameObject playerSpawnPoint;

    //персонаж игрока
    public string playerCharacter;

    public Bag newBag;

    //персонажи
    GameObject firstMember;
    [SerializeField]
    GameObject Valkar;
    [SerializeField]
    GameObject Kored;
    [SerializeField]
    GameObject Grundbuksa;
    [SerializeField]
    GameObject Riisje;

    //для вывода первого сообщения
    [SerializeField]
    GameObject messagePanel;
    MessageManager message;

    //другое
    CharacterLabelManager label;
    GroupManager group;
    UIManager UI;
    InventoryPanelManager inventory;

    void Awake()
    {
        UI = FindObjectOfType<UIManager>();
        group = FindObjectOfType<GroupManager>();
        inventory = FindObjectOfType<InventoryPanelManager>();
        LoadCharacter();
    }

    private void Start()
    {
        //того персонажа, которого выбрал игрок, помещаем в то место, откуда он начинает игру 
        if (playerCharacter == "Valkar")
        {
            label = Valkar.GetComponentInChildren<CharacterLabelManager>();
            Valkar.transform.position = playerSpawnPoint.transform.position;
            firstMember = Valkar;
            GameManager.CalebReputation += 1;
        }
        else if (playerCharacter == "Kored")
        {
            label = Kored.GetComponentInChildren<CharacterLabelManager>();
            Kored.transform.position = playerSpawnPoint.transform.position;
            firstMember = Kored;
            GameManager.CloudReputation += 1;
        }
        else if (playerCharacter == "Grundbuksa")
        {
            label = Grundbuksa.GetComponentInChildren<CharacterLabelManager>();
            Grundbuksa.transform.position = playerSpawnPoint.transform.position;
            firstMember = Grundbuksa;
            GameManager.EchoReputation += 1;
        }
        else if (playerCharacter == "Riise")
        {
            label = Riisje.GetComponentInChildren<CharacterLabelManager>();
            Riisje.transform.position = playerSpawnPoint.transform.position;
            firstMember = Riisje;
            GameManager.ShimrathReputation += 1;
        }

        LoadStats();
        firstMember.GetComponent<CharacterProfile>().ApplyStatistics();

        //не забываем удалить с него скрипт лейбла и сам лейбл
        //отключено на время теста
        //label.DestroyLabel();
        //Destroy(itemLabel);

        firstMember.tag = "Member";//присваиваем ему тег 

        //навешиваем на него необходимые скрипты и компоненты
        NavMeshAgent navMeshAgent = firstMember.AddComponent<NavMeshAgent>();
        navMeshAgent.radius = 1f;
        firstMember.GetComponent<CharacterProfile>().DialogEnable = false;//отключаем возможность завязать с ним диалог
        StartCoroutine(Intro());//ждем 0.1 секунду, чтобы успели отработать другие скрипты

        //и убираем ненужные компоненты
        Destroy(firstMember.GetComponent<NavMeshObstacle>());
    }

    void Update()
    {
        message = messagePanel.GetComponent<MessageManager>();
    }

    IEnumerator Intro()
    {
        yield return null;

        //помещаем основного персонажа в группу
        group.groupMembersCount = 1;
        group.members[0] = firstMember;
        UI.AddMemberInfo();

        //экипируем персонажа другой сумкой
        firstMember.GetComponent<CharacterEquipment>().bag = newBag;

        //обновляем инфу о лимите веса группы
        inventory.UpdateGroupWeight();

        //запускаем первое сообщение
        messagePanel.SetActive(true);//включаем панель
        UIManager.messagePanelIsActive = true;//помечаем ее как открытую
        //выводим сообщение, в зависимости от пола выбранного персонажа
        if (group.members[0].GetComponent<CharacterProfile>().CharacterGender == "male")
        {
            message.id = 0;
        }
        else
        {
            message.id = 1;
        }
    }

    void LoadCharacter()
    {
        playerCharacter = PlayerPrefs.GetString("playerCharacter");
    }

    void LoadStats()
    {
        firstMember.GetComponent<CharacterProfile>().StrBase = PlayerPrefs.GetInt("str");
        firstMember.GetComponent<CharacterProfile>().DexBase = PlayerPrefs.GetInt("dex");
        firstMember.GetComponent<CharacterProfile>().ConstBase = PlayerPrefs.GetInt("const");
        firstMember.GetComponent<CharacterProfile>().IntBase = PlayerPrefs.GetInt("int");
        firstMember.GetComponent<CharacterProfile>().PercBase = PlayerPrefs.GetInt("perc");
        //PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }





}
