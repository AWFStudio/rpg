﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.IO;

//висит на пустом элементе сцены кастомизации

public class CastomizationSceneManager : MonoBehaviour
{
    [Header("Кнопки")]
    [SerializeField]
    Text backBttnTxt;
    [SerializeField]
    Text nextBttnTxt;

    [Header("Панели")]
    [SerializeField]
    GameObject levelSelectionPanel;
    [SerializeField]
    GameObject characterSelectionPanel;

    [Header("Панель предупреждения")]
    [SerializeField]
    GameObject warningPanel;
    [SerializeField]
    Text outerWarningText;

    //прочее
    TextAsset asset;
    XMLSettings UIelement;
    LevelSelectionManager level;
    CharacterSelectionManager character;

    private void Awake()
    {
        level = FindObjectOfType<LevelSelectionManager>();
        character = FindObjectOfType<CharacterSelectionManager>();
    }

    void Start()
    {
        ApplyLanguageSettings();
    }

    //для кнопки "назад"
    public void BackBttn()
    {
        if (levelSelectionPanel.activeSelf)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
        else
        {
            characterSelectionPanel.SetActive(false);
            levelSelectionPanel.SetActive(true);
        }
    }

    //для кнопки "продолжить"
    public void ContinueBttn()
    {
        if (levelSelectionPanel.activeSelf)
        {
            if (level.SelectedLevel != 0)
            {
                levelSelectionPanel.SetActive(false);
                characterSelectionPanel.SetActive(true);
            }
            else
            {
                warningPanel.SetActive(true);
            }
        }
        else
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(2);
            //if (freeStats == 0)
            //{
            Save();
            //}
        }
    }

    void ApplyLanguageSettings()//локализация элементов UI
    {
        asset = Resources.Load<TextAsset>("Localization/" + LocalizationManager.currentLanguage + "/UI Elements/Castomization");
        UIelement = XMLSettings.Load(asset);

        //кнопки
        backBttnTxt.text = UIelement.UIelements[0].text;
        nextBttnTxt.text = UIelement.UIelements[1].text;

        //панель предупреждения
        outerWarningText.text = UIelement.UIelements[74].text;

    }

    public void Save()
    {
        PlayerPrefs.SetInt("selectedLevel", level.SelectedLevel);
        PlayerPrefs.SetString("playerCharacter", character.SelectedCharacter);
        //    //    PlayerPrefs.SetInt("str", strBase);
        //    //    PlayerPrefs.SetInt("dex", dexBase);
        //    //    PlayerPrefs.SetInt("const", constBase);
        //    //    PlayerPrefs.SetInt("int", intBase);
        //    //    PlayerPrefs.SetInt("perc", percBase);
        PlayerPrefs.Save();
    }
}
