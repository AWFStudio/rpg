﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//скрипт сцены загрузки

public class LoadingSceneManager : MonoBehaviour
{
    public Image loadingImg;//изображение, демонстрирующее прогресс загрузки (его Image Type должен быть Filled)
    public Text progressText;//текст, отражающий прогрксс загрузки 
    public int sceneID;//сцена, которую мы загружаем

    void Start()
    {
        StartCoroutine(AsyncLoad());
    }

    IEnumerator AsyncLoad()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneID);//производим загрузку указанной сцены
        while (!operation.isDone)//пока операция не завершена...
        {
            //поскольку по мере загрузки сцены, operation.progress доходит максимум до 0.9, а значение "единица" присваивает операции только после "перескакивания" на новую сцену, то мы не успеваем увидеть полностью загруженную картинку
            //поэтому, тут подставляем костыль
            float progress = operation.progress / 0.9f;
            loadingImg.fillAmount = progress;//степень заполнения изображения = проценту загрузки
            //указываем формат строки, которую будем выводить (поскольку формат % имеет тип float, то если не указать ":0", т.е. не округлить выводимую цифру до нуля, то будет выводиться дробное число
            progressText.text = string.Format("{0:0}%", progress*100);
            yield return null;//пропускаем кадр
        }
    }
}
